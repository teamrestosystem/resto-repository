/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Rococo Global Technologies, Inc - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
package resto.dto;

import java.security.Timestamp;
import java.util.List;

import org.slim3.datastore.Attribute;

import resto.model.MenuItemModel;

/**
 * Contains the properties and methods for the order model.
 * @author Team Rocket
 * @version 0.01
 * Version History
 * [08/10/2016] 0.01 � Loregge Kandt  � Initial codes.
 * [09/21/2016] 0.02 - Ryan L. Lobitana - re-aligning OrderDto
 * [10/16/2016] 0.02 - Keith Edmond L. Araneta - added property 'tableId'
 */
public class OrderDto extends BaseDto {
    /**
     * The id of the 'dish'.
     */
    private Long id;
    
    /**
     * Order�s number (can be referred to as receipt number)
     */    
    private Long orderNumber;
    
    /**
     * Table�s number
     */    
    private String tableNumber;
    
    /**
     * Date and time when the order was taken
     */
    private String orderTimestamp;
    
    /**
     * The list of ordered menu items
     */
    @Attribute(lob = true)
    private List<MenuItemDto> menuItems;
    
    /**
     * The total cost of all the ordered items
     */
    private double totalCost;
    
    /**
     * The status of the ordered items
     */
    private String status;    

    public String getOrderTimestamp() {
        return orderTimestamp;
    }

    public void setOrderTimestamp(String orderTimestamp) {
        this.orderTimestamp = orderTimestamp;
    }
       
    public Long getOrderNumber() {
        return orderNumber;
    }
    
    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }
    
    public List<MenuItemDto> getMenuItems() {
        return menuItems;
    }
    

    public void setMenuItems(List<MenuItemDto> menuItems) {
        this.menuItems = menuItems;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getTableNumber() {
        return tableNumber;
    }
    
    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }
}
