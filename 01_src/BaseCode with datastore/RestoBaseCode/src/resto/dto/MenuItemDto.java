/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Contains the properties and methods for the menu item model.
 * @author Team Rocket
 * @version 0.02
 * Version History
 * [08/10/2016] 0.01 � Loregge Kandt  � Initial codes.
 * [08/10/2016] 0.02 � Keith Edmond L. Araneta  � Added the properties(variables) of the
 *                                                "Menu Item" entity; getters and setters.
 * [09/21/2016] 0.02 � Keith Edmond L. Araneta - Added property 'category' of "Menu Item"
 * [10/05/2016] 0.02 � Keith Edmond L. Araneta - Added property 'imagePath' of "Menu Item"                                              
 */
public class MenuItemDto extends BaseDto implements Serializable {
    /**
     * The id of the 'Menu Item'.
     */
    private Long id;
    
    /**
     * The name of the 'Menu Item'
     */
    private String menuItemName;
    
    /**
     * The number of available servings of the 'Menu Item'
     */
    private int servings;
    
    /**
     * The price per serving of the 'Menu Item'
     */
    private double price;
    
    /**
     * The status of the 'Menu Item'; (available or unavailable)
     */
    private String status;
    
    /**
     * The category of the 'Menu Item';
     */
    private String category;
    
    /**
     * Retrieves the id of the 'Menu Item'.
     * @return the id of the 'Menu Item'.
     */
    
    /**
     * The path of the image of the 'Menu Item';
     */
    private String imagePath;
    
    /**
     * Retrieves the id of the 'Menu Item'.
     * @return the id of the 'Menu Item'.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id of the 'Menu Item'.
     * @param id - the value to be set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Retrieves the name of the 'Menu Item'.
     * @return the name of the 'Menu Item'.
     */
    public String getMenuItemName() {
        return menuItemName;
    }

    /**
     * Sets the name of the 'Menu Item'
     * @param menuItemName - the value to be set
     */
    public void setMenuItemName(String menuItemName) {
        this.menuItemName = menuItemName;
    }

    /**
     * Retrieves the number of available servings of the 'Menu Item'.
     * @return the number of available servings of the 'Menu Item'.
     */
    public int getServings() {
        return servings;
    }

    /**
     * Sets the number of available servings of the 'Menu Item'.
     * @param servings - the value to be set
     */
    public void setServings(int servings) {
        this.servings = servings;
    }

    /**
     * Retrieves the price of the 'Menu Item'.
     * @return the price of the 'Menu Item'.
     */
    public double getPrice() {
        return price;
    }

    /**
     * Sets the price of the 'Menu Item'.
     * @param price - the value to be set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Retrieves the status of the 'Menu Item'; (available or unavailable).
     * @return the status of the 'Menu Item'.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the status of the 'Menu Item'; (available or unavailable).
     * @param status - the value to be set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
    /**
     * Retrieves the category of the 'Menu Item'
     * @return the category of the 'Menu Item'.
     */
    public String getCategory() {
        return category;
    }
    
    /**
     * Sets the category of the 'Menu Item'
     * @param category - the value to be set
     */
    public void setCategory(String category) {
        this.category = category;
    }
    
    /**
     * Retrieves the path of the image of the 'Menu Item'
     * @return the path of the image of the 'Menu Item'.
     */
    public String getImagePath() {
        return imagePath;
    }
    
    /**
     * Sets the path of the image of the 'Menu Item'
     * @param imagePath - the value to be set
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
