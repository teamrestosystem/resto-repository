/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains the DTO for the list of MenuItem entity.
 * @author Team Rocket
 * @version 0.01
 * Version History
 * [09/21/2016] 0.01 � Keith Edmond L. Araneta  � Initial codes.
 */
public class MenuItemListDto extends BaseDto {
    
    /**
     * List of menu item dtos.
     */
    private List<MenuItemDto> menuItems = new ArrayList<MenuItemDto>();
    
    /**
     * Retrieve menu item list.
     * @return menu item list.
     */
    public List<MenuItemDto> getEntries() {
        return menuItems;
    }
    
    /**
     * Sets menu item list.
     * @param menuItems the menu item list to set.
     */
    public void setEntries(List<MenuItemDto> menuItems) {
        this.menuItems = menuItems;
    }
}
