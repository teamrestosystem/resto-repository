/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Rococo Global Technologies, Inc - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
package resto.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains the properties and methods for the order model.
 * @author Team Rocket
 * @version 0.01
 * Version History
 * [09/26/2016] 0.01 � Teresita Tala P. Rabago  � Initial codes.
 * [09/29/2016] 0.02 � Teresita Tala P. Rabago � Updated some codes
 */

public class TableListDto extends BaseDto{
    /**
     * List of table item dtos.
     */
    private List<TableDto> tableItems = new ArrayList<TableDto>();
    
    /**
     * Retrieve table item list.
     * @return table item list.
     */
    public List<TableDto> getEntries() {
        return tableItems;
    }
    
    /**
     * Sets table item list.
     * @param tableItems - the table item list to set.
     */
    public void setEntries(List<TableDto> tableItems) {
        this.tableItems = tableItems;
    }
}
