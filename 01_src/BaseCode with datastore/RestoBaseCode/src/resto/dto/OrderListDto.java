/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Rococo Global Technologies, Inc - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
package resto.dto;

/**
 * Contains the properties and methods for the order model.
 * @author Team Rocket
 * @version 0.01
 * Version History
 * [09/26/2016] 0.01 � Teresita Tala P. Rabago  � Initial codes.
 * [09/29/2016] 0.02 � Teresita Tala P. Rabago � Updated some codes
 */

import java.util.ArrayList;
import java.util.List;

public class OrderListDto extends BaseDto{
    /**
     * List of order item dtos.
     */
    private List<OrderDto> orderItems = new ArrayList<OrderDto>();
    
    /**
     * Retrieve order item list.
     * @return order item list.
     */
    public List<OrderDto> getEntries() {
        return orderItems;
    }
    
    /**
     * Sets order item list.
     * @param orderItems - the order item list to set.
     */
    public void setEntries(List<OrderDto> orderItems) {
        this.orderItems = orderItems;
    }
}
