/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Rococo Global Technologies, Inc - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
package resto.dto;

/**
 * Contains the properties and methods for the order model.
 * @author Team Rocket
 * @version 0.01
 * Version History
 * [08/10/2016] 0.01 � Loregge Kandt  � Initial codes.
 * [09/28/2016] 0.02 � Teresita Tala P. Rabago  � Some codes.
 * [09/29/2016] 0.03 � Teresita Tala P. Rabago  � Changed Some codes.
 */
public class TableDto extends BaseDto{
    /**
     * The id of the 'table'.
     */
    private Long id;
    
    /**
     * The number of the 'table'.
     */
    private String tableNumber;
    
    /**
     * The capacity of the 'table'.
     */
    private int tableCapacity;
    
    /**
     * The status of the 'table'.
     */
    private String tableStatus;
    
    /**
     * Retrieves the id of the 'table'.
     * @return the id of the 'table'.
     */
    public Long getId() {
        return id;
    }
    
    /**
     * Sets the id of the 'table'
     * @param id - the value to be set
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * Retrives the number of the 'table'.
     * @return the number of the 'table'.
     */
    public String getTableNumber() {
        return tableNumber;
    }
    
    /**
     * Sets the number of the 'table'.
     * @param tableNo - the value to be set
     */
    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }
    
    /**
     * Retrieves the capacity of the 'table'
     * @return the capacity of the 'table'
     */
    public int getTableCapacity() {
        return tableCapacity;
    }
    
    /**
     * Sets the capacity of the 'table'
     * @param tableCapacity - the value to be set
     */
    public void setTableCapacity(int tableCapacity) {
        this.tableCapacity = tableCapacity;
    }
    
    /**
     * Retrieves the status of the 'table'.
     * @return the status of the 'table'.
     */
    public String getTableStatus() {
        return tableStatus;
    }
    
    /**
     * Sets the status of the 'table'.
     * @param tableStatus - the value to be set.
     */
    public void setTableStatus(String tableStatus) {
        this.tableStatus = tableStatus;
    } 
}
