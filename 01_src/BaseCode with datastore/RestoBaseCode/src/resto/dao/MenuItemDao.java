/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.dao;

import java.util.ArrayList;
import java.util.List;

import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;

import resto.meta.MenuItemModelMeta;
import resto.model.MenuItemModel;

/**
 * Contains the functions that will be used to access the
 * 'Menu Item' entity from datastore.
 * @author Team Rocket
 * @version 0.02
 * Version History
 * [08/10/2016] 0.01 - Loregge M. Kandt - Initial codes.
 * [08/10/2016] 0.02 - Keith Edmond L. Araneta - Added and implemented functions
 *                                               getMenuItemByName() and insertMenuItem().
 * [08/31/2016] 0.02 - Keith Edmond L. Araneta - Added and implemented functions
 *                                               getMenuItemById() and deleteMenuItem().
 * [08/10/2016] 0.03 - Teresita Tala P. Rabago - implemented updateMenuItem() and getMenuItemById()
 *                                               functions
 * [09/21/2016] 0.03 - Keith Edmond L. Araneta - Added and implemented getMenuItemList()
 */

public class MenuItemDao {
    
    /**
     * This function is used to retrieve all the entities from the datastore.
     * 
     * @return list of menuItems
     */
    public List<MenuItemModel> getMenuItemList(){
        List<MenuItemModel> menuItemModelList = new ArrayList<MenuItemModel>();
        MenuItemModelMeta meta = MenuItemModelMeta.get();
        menuItemModelList = Datastore.query(meta).asList();
        return menuItemModelList;
    }
    
    /**
     * This function is used to query a 'Menu Item' with the same id.
     *
     * @param inputModel
     *            - contains the id to be checked.
     * @return model that was returned by the query.
     */
    public MenuItemModel getMenuItemById(MenuItemModel inputModel) {
        MenuItemModel menuItemModel = new MenuItemModel();

        MenuItemModelMeta e = MenuItemModelMeta.get();
        menuItemModel = Datastore.query(e)
                          .filter(e.id.equal(inputModel.getId())).asSingle();

        return menuItemModel;
    }

    /**
     * Used to query a 'Menu Item' that has the same name.
     * @param inputMenuItem - contains the 'name' to be queried
     * @return MenuItemModel - returned by the query.
     */
    public MenuItemModel getMenuItemByName(MenuItemModel inputMenuItem) {
        // container for the query result
        MenuItemModel menuItemModel = new MenuItemModel();

        // use the meta object for filtering the results
        MenuItemModelMeta meta = MenuItemModelMeta.get();
        menuItemModel = Datastore.query(meta)
                .filter(meta.menuItemName.equal(inputMenuItem.getMenuItemName()))
                .asSingle();

        return menuItemModel;
    }

    /**
     * Use to insert a 'Menu Item' to the datastore.
     * @param inputMenuItem - the entity to be added/updated.
     */
    public void insertMenuItem(MenuItemModel inputMenuItem) {
        Transaction transaction = Datastore.beginTransaction();
        // creating key and ID for the new entity
        Key parentKey = KeyFactory.createKey("MenuItem", inputMenuItem.getMenuItemName());
        Key key = Datastore.allocateId(parentKey, "MenuItemModel");

        // setting the 'key' and 'id' of the model
        inputMenuItem.setKey(key);
        inputMenuItem.setId(key.getId());

        // inserting the data into the datastore
        Datastore.put(inputMenuItem);
        transaction.commit();
    }

    /**
     * This function is used to delete a 'Menu Item' from the datastore.
     *
     * @param inputModel
     *            - model to be deleted
     */
    public void deleteMenuItem(MenuItemModel inputModel) {
        Transaction transaction = Datastore.beginTransaction();

        Datastore.delete(inputModel.getKey());

        transaction.commit();
    }

    /**
     * This function is used to update a 'Menu Item' to the datastore.
     * @param inputModel
     *            - model to be updated
     */
    public void updateMenuItem(MenuItemModel inputModel) {
        Transaction transaction = Datastore.beginTransaction();

        Datastore.put(inputModel);

        transaction.commit();
    }
}
