/* Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.dao;

import java.util.ArrayList;
import java.util.List;

import org.slim3.datastore.Datastore;

import resto.meta.TableModelMeta;
import resto.model.TableModel;


import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;

/**
 * Contains the functions that will be used to access the
 * 'Menu Item' entity from datastore.
 * @author Team Rocket
 * @version 0.02
 * Version History
 * [09/26/2016] 0.01 - Teresita Tala P. Rabago - Initial codes.
 * [09/29/2016] 0.02 � Teresita Tala P. Rabago � Updated some codes
 *                                             - Implemented getTableItemByTableNumber(TableModel inputModel) function
 *                                             - Implemented insertTableItem(TableModel inputTableItem) function
 * [10/16/2016] 0.03 � Loregge M. Kandt - Added updateTable function
*/

public class TableDao {
    
    DatastoreService dataService = DatastoreServiceFactory.getDatastoreService();
    
    /**
     * This function is used to retrieve all the entities from the datastore.
     * 
     * @return list of orderItems
     */
    
    public List<TableModel> getTableList(){
        List<TableModel> tableModelList = new ArrayList<TableModel>();
        TableModelMeta meta = TableModelMeta.get();
        tableModelList = Datastore.query(meta).asList();
        return tableModelList;
    }
    
    /**
     * This function is used to query a 'Table Item' with the same id.
     *
     * @param inputModel
     *            - contains the id to be checked.
     * @return model that was returned by the query.
     */
    public TableModel getTableByTableNumber(TableModel inputModel) {
        // container for the query result
        TableModel tableModel = new TableModel();
        
        // use the meta object for filtering the results
        TableModelMeta e = TableModelMeta.get();
        tableModel = Datastore.query(e)
                          .filter(e.tableNumber.equal(inputModel.getTableNumber())).asSingle();
        return tableModel;
    }
    
    /**
     * Use to insert a 'Table Item' to the datastore.
     * @param inputTableItem - the entity to be added/updated.
     */
    public void insertTable(TableModel inputTable) {
        Transaction transaction = Datastore.beginTransaction();
        // creating key and ID for the new entity
        Key parentKey = KeyFactory.createKey("TableItem", inputTable.getTableNumber());
        Key key = Datastore.allocateId(parentKey, "TableModel");

        // setting the 'key' and 'id' of the model
        inputTable.setKey(key);
        inputTable.setId(key.getId());

        // inserting the data into the datastore
        Datastore.put(inputTable);
        transaction.commit();
    }
    
    /**
     * This function is used to query a 'Table' with the same id.
     *
     * @param inputModel
     *            - contains the id to be checked.
     * @return model that was returned by the query.
     */
    public TableModel getTableById(TableModel inputModel) {
        System.out.println("TableDao.getTableById " + "start");
        TableModel tableModel = new TableModel();

        TableModelMeta e = TableModelMeta.get();
        tableModel = Datastore.query(e)
                          .filter(e.id.equal(inputModel.getId())).asSingle();

        System.out.println("TableDao.getTableById " + "end");
        return tableModel;
    }
    
    /**
     * This function is used to update a 'Table' to the datastore.
     * @param inputModel
     *            - model to be updated
     */
    public void updateTable(TableModel inputModel) {
        Transaction transaction = Datastore.beginTransaction();

        Datastore.put(inputModel);

        transaction.commit();
    }
}
