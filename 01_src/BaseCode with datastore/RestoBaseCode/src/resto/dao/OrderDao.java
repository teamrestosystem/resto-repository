package resto.dao;

import java.util.ArrayList;
import java.util.List;

import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import resto.meta.OrderModelMeta;
import resto.meta.TableModelMeta;
import resto.model.OrderModel;
import resto.model.TableModel;
    /**
     * Contains the functions that will be used to access the
     * 'Order' entity from datastore.
     * @author Team Rocket
     * @version 0.02
     * Version History
     * [09/21/2016] 0.01 - Ryan L. Lobitana- Initial codes.
     * [09/29/2016] 0.02 � Teresita Tala P. Rabago � Updated some codes
     *                                         
     */
public class OrderDao {
        
    DatastoreService dataService = DatastoreServiceFactory.getDatastoreService();

    /**
     * This function is used to retrieve all the entities from the datastore.
     * 
     * @return list of orderItems
     */
    public List<OrderModel> getOrderItemList(){
        List<OrderModel> orderItemModelList = new ArrayList<OrderModel>();
        OrderModelMeta meta = OrderModelMeta.get();
        orderItemModelList = Datastore.query(meta).asList();
        return orderItemModelList;
    }
    
    /**
     * This function is use as a orderModel with the same id.
     *
     * @param inputModel
     *            - contains the id to be checked.
     * @return model that was returned by the query.
     */
    public OrderModel getOrderById(OrderModel inputModel) {
        OrderModel orderModel = new OrderModel();

        OrderModelMeta e = OrderModelMeta.get();
        orderModel = Datastore.query(e)
                          .filter(e.id.equal(inputModel.getId())).asSingle();

        return orderModel;
    }

    /**
     * Used to query a OrderModel that has the same 'order number'.
     * @param inputOrder - contains the 'order number' to be queried
     * @return OrderModel - returned by the query.
     */
    public OrderModel getOrderByOrderNumber(OrderModel inputOrder) {
        // container for the query result
        OrderModel orderItemModel = new OrderModel();

        // use the meta object for filtering the results
        OrderModelMeta meta = OrderModelMeta.get();
        orderItemModel = Datastore.query(meta)
                .filter(meta.orderNumber.equal(inputOrder.getOrderNumber()))
                .asSingle();
        return orderItemModel;
    }

    /**
     * Use to insert a 'Order' to the datastore.
     * @param inputOrder - the entity to be added/updated.
     */
    public void insertOrder(OrderModel inputOrder) {
        Transaction transaction = Datastore.beginTransaction();
        // creating key and ID for the new entity
        Key parentKey = KeyFactory.createKey("Order", inputOrder.getOrderNumber());
        Key key = Datastore.allocateId(parentKey, "OrderModel");

        // setting the 'key' and 'id' of the model
        inputOrder.setKey(key);
        inputOrder.setId(key.getId());

        // inserting the data into the datastore\
        Datastore.put(inputOrder);
        transaction.commit();
    }

    /**
     * This function is used to delete a menuItemModel from the datastore.
     *
     * @param inputModel
     *            - model to be deleted
     */
    public void deleteOrder(OrderModel inputModel) {
        Transaction transaction = Datastore.beginTransaction();

        Datastore.delete(inputModel.getKey());

        transaction.commit();
    }

    /**
     * This function is used to update a MenuItemModel to the datastore.
     * @param inputModel
     *            - model to be updated
    */
    public void updateOrder(OrderModel inputModel) {
        Transaction transaction = Datastore.beginTransaction();

        Datastore.put(inputModel);

        transaction.commit();
    } 
}
