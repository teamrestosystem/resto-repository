/* Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.service;

import java.util.List;

import com.google.appengine.api.datastore.Entity;

import resto.common.GlobalConstants;
import resto.dao.TableDao;
import resto.dto.TableDto;
import resto.dto.OrderDto;
import resto.dto.OrderListDto;
import resto.dto.TableListDto;
import resto.model.TableModel;
import resto.model.OrderModel;

/**
* Contains the functions for inserting an entity to the datastore.
* @author Team Rocket
* @version 0.02
* Version History
* [09/28/2016] 0.01 � Teresita Tala P. Rabago � Initial codes.
* [09/29/2016] 0.02 � Teresita Tala P. Rabago � Updated TableListDto getTableItemList() function.
*                                             - Implemented insertTableItem(TableDto inputTableItem) function.
* [10/16/2016] 0.03 � Loregge M. Kandt - Added updateTable function
*/

public class TableService {
    /**
     * Use to access the DAO functions for the TableModel.
     */
    TableDao tableDao = new TableDao();
    
    public TableListDto getTableItemList() {
        
     // initializing the dto to hold the list of todos.
        TableListDto tableListDto =  new TableListDto();
        
        try {
            List<TableModel> tableItemList = tableDao.getTableList();
            if (tableItemList != null) {
                // convert each TableModel from the TableList into TableDto
                for (TableModel resultModel : tableItemList) {
                    TableDto tableItemDto = new TableDto();
                    
                    tableItemDto.setId(resultModel.getId()); 
                    tableItemDto.setTableNumber(resultModel.getTableNumber());
                    tableItemDto.setTableCapacity(resultModel.getTableCapacity());
                    tableItemDto.setTableStatus(resultModel.getTableStatus());
                    
                    // adding the dto to the list
                    tableListDto.getEntries().add(tableItemDto);
                }
            }
        }catch(Exception e) {
            tableListDto.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
        }
        
        return tableListDto;
    }
    
    /**
     * Used to insert an item to the datastore.
     * @param inputTableItem - the dto that contains the data to be stored
     *      in the model object.
     */
    public TableDto insertTable(TableDto inputTableItem) {
        /**
         * Used to store the data from the DTO object to the model.
         * Used as parameters in passing to the DAO layer.
         */
        TableModel tableItemModel = new TableModel();

        // setting the data of the model from the dto
        tableItemModel.setId(inputTableItem.getId());
        tableItemModel.setTableNumber(inputTableItem.getTableNumber());
        tableItemModel.setTableCapacity(inputTableItem.getTableCapacity());
        tableItemModel.setTableStatus(inputTableItem.getTableStatus());

        try {
            // check if there is already an existing "Table Item"
            TableModel resultModel = this.tableDao.getTableByTableNumber(tableItemModel);

            // check if item doesn't exists
            if (null != resultModel) {
                // Item must not be inserted
                inputTableItem.addError("Cannot add table item. An item with the same id already exists.");
            } else {
                // add the item to the datastore
                try {
                    this.tableDao.insertTable(tableItemModel);
                } catch (Exception e) {
                    inputTableItem.addError("Exception in adding table item: " + e.toString());
                }
            }
        } catch (Exception e) {
            inputTableItem.addError("Exception in adding table item: " + e.toString());
        }
        
        return inputTableItem;
    }
    
    /**
     * Used to update the Table entity in the datastore.
     * @param inputTable - contains the data that will be store to the model.
     */
    public TableDto updateTable(TableDto inputTable) {
        /**
         * TableModel that will be stored to the datastore.
         */
        TableModel tableModel = storeDtoToModel(inputTable);

        try {
            // checking if menu item already exists in the datastore.
            TableModel resultModel = tableDao.getTableByTableNumber(tableModel);

            if (resultModel != null) {
                // setting the key in order to properly update the item
                tableModel.setKey(resultModel.getKey());
                // update the entity to the datastore.
                this.tableDao.updateTable(tableModel);            
            }
        } catch (Exception e) {
            inputTable.addError("Exception in updating table: " + e.toString());
        }
        
        return inputTable;
    }
    
    public TableModel storeDtoToModel(TableDto tableDto) {
        // Used to store the data from the DTO object.
        TableModel tableModel = new TableModel();

        // Storing the data from the DTO.
        tableModel.setId(tableDto.getId());
        tableModel.setTableNumber(tableDto.getTableNumber());
        tableModel.setTableCapacity(tableDto.getTableCapacity());
        tableModel.setTableStatus(tableDto.getTableStatus());

        // returning the model
        return tableModel;
    }

}
