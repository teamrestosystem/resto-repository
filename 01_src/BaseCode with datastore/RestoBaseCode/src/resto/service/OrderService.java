/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.service;

import java.util.List;

import resto.common.GlobalConstants;
import resto.dao.OrderDao;
import resto.dto.OrderDto;
import resto.dto.OrderListDto;
import resto.model.OrderModel;

/**
* Contains the functions for inserting an entity to the datastore.
* @author Team Rocket
* @version 0.02
* Version History
* [09/21/2016] 0.01 � Ryan L. Lobitana � Initial codes.
* [09/29/2016] 0.02 � Teresita Tala P. Rabago � Updated OrderListDto getOrderItemList() function.
* [10/04/2016] 0.02 � Teresita Tala P. Rabago � Updated some codes.
* [10/08/2016] 0.02 - Teresita Tala P. Rabago - Updated some codes.
* [10/16/2016] 0.02 - Keith Edmond L. Araneta - minor changes 
*/

public class OrderService {
    /**
     * Use to access the DAO functions for the MenuItemModel.
     */
    OrderDao orderDao = new OrderDao();
    

    /**
     * Used to insert an order to the datastore.
     * @param inputOrder - the dto that contains the data to be stored
     *      in the model object.
     */
    public OrderDto insertOrder(OrderDto inputOrder) {
        /**
         * Used to store the data from the DTO object to the model.
         * Used as parameters in passing to the DAO layer.
         */
        OrderModel orderModel = new OrderModel();

        // setting the data of the model from the dto
        orderModel.setId(inputOrder.getId());
        orderModel.setOrderNumber(inputOrder.getOrderNumber());
        orderModel.setTableNumber(inputOrder.getTableNumber());
        orderModel.setOrderTimestamp(inputOrder.getOrderTimestamp());
        orderModel.setStatus(inputOrder.getStatus());
        orderModel.setTotalCost(inputOrder.getTotalCost());
        orderModel.setMenuItems(inputOrder.getMenuItems());;

        try {
            // check if there is already an existing "Order"
            OrderModel resultModel = this.orderDao.getOrderByOrderNumber(orderModel);

            // item doesn't exists
            if (null != resultModel) {
                // Order must not be inserted
                inputOrder.addError("There is already an order with same order number");
            } else {
                // add the order to the datastore
                try {
                    this.orderDao.insertOrder(orderModel);
                } catch (Exception e) {
                    inputOrder.addError("Exception in Adding Order " + e.toString());
                }
            }
        } catch (Exception e) {
            inputOrder.addError("Exception in Adding Order " + e.toString());
        }

        return inputOrder;
    }

    public OrderDto deleteOrder(OrderDto inputOrder) {
        /**
         * Used to store the data from the DTO object to the model.
         * Used as parameters in passing to the DAO layer.
         */
        OrderModel orderModel = new OrderModel();

        orderModel.setId(inputOrder.getId());

        try {
            OrderModel resultModel = orderDao.getOrderById(orderModel);

            if (resultModel != null) {
                orderModel.setKey(resultModel.getKey());
                this.orderDao.deleteOrder(orderModel);
            } 
        } catch (Exception e) {
            inputOrder.addError("Exception in deleting order " + e.toString());
        }

        return inputOrder;
    }
    
    public OrderListDto getOrderItemList() {
        
     // initializing the dto to hold the list of todos.
        OrderListDto orderListDto =  new OrderListDto();
        
        try {
            List<OrderModel> orderItemList = orderDao.getOrderItemList();
            if (orderItemList != null) {
                // convert each orderItemModel from the orderItemList into orderItemDto
                for (OrderModel resultModel : orderItemList) {
                    OrderDto orderItemDto = new OrderDto();
                    
                    orderItemDto.setId(resultModel.getId());
                    orderItemDto.setOrderNumber(resultModel.getOrderNumber());
                    orderItemDto.setTableNumber(resultModel.getTableNumber());
                    orderItemDto.setMenuItems(resultModel.getMenuItems());
                    orderItemDto.setOrderTimestamp(resultModel.getOrderTimestamp());
                    orderItemDto.setStatus(resultModel.getStatus());
                    orderItemDto.setTotalCost(resultModel.getTotalCost());
                    
                    // adding the dto to the list
                    orderListDto.getEntries().add(orderItemDto);
                }
            }
        }catch(Exception e) {
            orderListDto.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
        }
        
        return orderListDto;
    }
    
    public OrderModel storeDtoToModel(OrderDto orderDto) {
        // Used to store the data from the DTO object.
        OrderModel orderModel = new OrderModel();

        // Storing the data from the DTO.
        orderModel.setId(orderDto.getId());
        orderModel.setMenuItems(orderDto.getMenuItems());
        orderModel.setOrderNumber(orderDto.getOrderNumber());
        orderModel.setTableNumber(orderDto.getTableNumber());
        orderModel.setOrderTimestamp(orderDto.getOrderTimestamp());
        orderModel.setTotalCost(orderDto.getTotalCost());
        orderModel.setStatus(orderDto.getStatus());

        // returning the model
        return orderModel;
    }

    /**
     * Used to update the MenuItem entity to the datastore.
     * @param menuItemDto - contains the data that will be store to the model.
     */
    public OrderDto updateOrder(OrderDto orderDto) {
        /**
         * OrderModel that will be stored to the datastore.
         */
        OrderModel orderModel = storeDtoToModel(orderDto);

        try {
            // checking if there is already the same item that exists in the datastore.
            OrderModel orderModel2 = orderDao.getOrderById(orderModel);

            if (orderModel2 != null) {
                // setting the key in order to properly update the item
                orderModel.setKey(orderModel2.getKey());
                // update the entity to the datastore.
                this.orderDao.updateOrder(orderModel);
            }
        } catch (Exception e) {
            orderDto.addError("Exception in updating order: " + e.toString());
        }
        System.out.println("OrderService.updateOrder " + "end");
        
        return orderDto;
    }
}
