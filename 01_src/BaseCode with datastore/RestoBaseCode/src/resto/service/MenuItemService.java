/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.service;

import java.util.List;

import com.google.appengine.api.datastore.Entity;

import resto.common.GlobalConstants;
import resto.dao.MenuItemDao;
import resto.dto.MenuItemDto;
import resto.dto.MenuItemListDto;
import resto.model.MenuItemModel;

/**
 * Contains the functions for inserting an entity to the datastore.
 * @author Team Rocket
 * @version 0.02
 * Version History
 * [08/10/2016] 0.01 � Loregge M. Kandt  � Initial codes.
 * [08/10/2016] 0.02 � Keith Edmond L. Araneta  � Added and implemeted function insertMenuItem().
 * [08/13/2016] 0.02 � Keith Edmond L. Araneta  � Added and implemeted function deleteMenuItem().
 * [08/31/2016] 0.02 � Teresita Tala P. Rabago  � Implemeted updateMenuItem()
 * [09/21/2016] 0.02 � Keith Edmond L. Araneta  � Updated insertMenuItem() function
 *                                              - Added and implemented getMenuItemList() and
 *                                                getMenuItemList(menuItem)
 * [09/28/2016] 0.02 � Keith Edmond L. Araneta  - Removed getMenuItemList(menuItem) function
 *                                                and updated getMenuItemList(), and updateMenuItem()
 * [10/05/2016] 0.02 - Keith Edmond L. Araneta  - Updated code for the new 'imagePath' property of 'menuItem'                                           
 */

public class MenuItemService {
    /**
     * Use to access the DAO functions for the MenuItemModel.
     */
    MenuItemDao menuItemDao = new MenuItemDao();

    /**
     * Used to insert an item to the datastore.
     * @param inputMenuItem - the dto that contains the data to be stored
     *      in the model object.
     */
    public MenuItemDto insertMenuItem(MenuItemDto inputMenuItem) {
        /**
         * Used to store the data from the DTO object to the model.
         * Used as parameters in passing to the DAO layer.
         */
        MenuItemModel menuItemModel = new MenuItemModel();

        // setting the data of the model from the dto
        menuItemModel.setId(inputMenuItem.getId());
        menuItemModel.setMenuItemName(inputMenuItem.getMenuItemName());
        menuItemModel.setServings(inputMenuItem.getServings());
        menuItemModel.setPrice(inputMenuItem.getPrice());
        menuItemModel.setStatus(inputMenuItem.getStatus());
        menuItemModel.setCategory(inputMenuItem.getCategory());
        menuItemModel.setImagePath(inputMenuItem.getImagePath());

        try {
            // check if there is already an existing "Menu Item"
            MenuItemModel resultModel = this.menuItemDao.getMenuItemByName(menuItemModel);

            // check if item doesn't exists
            if (null != resultModel) {
                // Item must not be inserted
                inputMenuItem.addError("Cannot add menu item. An item with the same name already exists.");
            } else {
                // add the item to the datastore
                try {
                    this.menuItemDao.insertMenuItem(menuItemModel);
                } catch (Exception e) {
                    inputMenuItem.addError("Exception in adding menu item: " + e.toString());
                }
            }
        } catch (Exception e) {
            inputMenuItem.addError("Exception in adding menu item: " + e.toString());
        }
        
        return inputMenuItem;
    }

    public MenuItemDto deleteMenuItem(MenuItemDto inputMenuItem) {
        /**
         * Used to store the data from the DTO object to the model.
         * Used as parameters in passing to the DAO layer.
         */
        MenuItemModel menuItemModel = new MenuItemModel();

        menuItemModel.setId(inputMenuItem.getId());

        try {
            MenuItemModel resultModel = menuItemDao.getMenuItemById(menuItemModel);

            if (resultModel != null) {
                menuItemModel.setKey(resultModel.getKey());
                this.menuItemDao.deleteMenuItem(menuItemModel);
            } 
        } catch (Exception e) {
            inputMenuItem.addError("Exception in deleting menu item: " + e.toString());
        }

        return inputMenuItem;
    }

    /**
     * Used to update the MenuItem entity in the datastore.
     * @param inputMenuItem - contains the data that will be store to the model.
     */
    public MenuItemDto updateMenuItem(MenuItemDto inputMenuItem) {
        /**
         * MenuItemModel that will be stored to the datastore.
         */
        MenuItemModel menuItemModel = storeDtoToModel(inputMenuItem);

        try {
            // checking if menu item already exists in the datastore.
            MenuItemModel resultModel = menuItemDao.getMenuItemById(menuItemModel);

            if (resultModel != null) {
                // setting the key in order to properly update the item
                menuItemModel.setKey(resultModel.getKey());
                // update the entity to the datastore.
                this.menuItemDao.updateMenuItem(menuItemModel);
            }
        } catch (Exception e) {
            inputMenuItem.addError("Exception in updating menu item: " + e.toString());
        }
        
        return inputMenuItem;
    }
    
    public MenuItemListDto getMenuItemList() {
        // initializing the dto to hold the list of todos.
        MenuItemListDto menuItemListDto =  new MenuItemListDto();
        
        try {
            List<MenuItemModel> menuItemList = menuItemDao.getMenuItemList();
            if (menuItemList != null) {
                // convert each menuItemModel from the menuItemList into menuItemDto
                for (MenuItemModel resultModel : menuItemList) {
                    MenuItemDto menuItemDto = new MenuItemDto();
                    
                    menuItemDto.setId(resultModel.getId()); 
                    menuItemDto.setMenuItemName(resultModel.getMenuItemName());
                    menuItemDto.setServings(resultModel.getServings());
                    menuItemDto.setPrice(resultModel.getPrice());
                    menuItemDto.setStatus(resultModel.getStatus());
                    menuItemDto.setCategory(resultModel.getCategory());
                    menuItemDto.setImagePath(resultModel.getImagePath());
                    
                    // adding the dto to the list
                    menuItemListDto.getEntries().add(menuItemDto);
                }
            }
        }catch(Exception e) {
            menuItemListDto.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
        }
        
        return menuItemListDto;
    }
    
    public MenuItemModel storeDtoToModel(MenuItemDto menuItemDto) {
        // Used to store the data from the DTO object.
        MenuItemModel menuItemModel = new MenuItemModel();

        // Storing the data from the DTO.
        menuItemModel.setId(menuItemDto.getId());
        menuItemModel.setMenuItemName(menuItemDto.getMenuItemName());
        menuItemModel.setServings(menuItemDto.getServings());
        menuItemModel.setPrice(menuItemDto.getPrice());
        menuItemModel.setStatus(menuItemDto.getStatus());
        menuItemModel.setCategory(menuItemDto.getCategory());
        menuItemModel.setImagePath(menuItemDto.getImagePath());

        // returning the model
        return menuItemModel;
    }
}
