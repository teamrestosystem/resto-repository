/* Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import resto.common.GlobalConstants;
import resto.dto.TableDto;
import resto.service.TableService;
import resto.utils.JSONValidators;

/**
 * Controller used to insert a 'Menu Item' to the datastore.
 * @author Team Rocket
 * @version 0.02
 * Version History
 * [08/10/2016] 0.01 � Loregge M. Kandt � Initial codes.
 * [09/29/2016] 0.02 � Teresita Tala P. Rabago � Implemented run function.
 */

public class AddTableController extends Controller {

    /**
     * Service object that will be used to call the insert function to datastore.
     */
    TableService tableService = new TableService();
    
    /**
     * The function that will be ran first upon entering this controller.
     * Used to insert a 'Menu Item' entity to the datastore.
     */
    @Override
    public Navigation run() throws Exception {
        /**
         * Used to store the information from the request and send to the
         * service class.
         */
        TableDto tableItemDto = new TableDto();
        /**
         * Holds the data sent from the request of the client side.
         */
        JSONObject jsonObject= null;
        
        try {
            jsonObject = new JSONObject(this.request.getReader().readLine());
            JSONValidators validator = new JSONValidators(jsonObject);
            
            validator.add("tableNumber", validator.required());
            validator.add("tableCapacity", validator.required());
            validator.add("tableStatus", validator.required());
            
            if(validator.validate()) {
                // add into the datastore
                
                // Getting all the information sent from the request.
                tableItemDto.setTableNumber(jsonObject.getString("tableNumber"));
                tableItemDto.setTableCapacity(jsonObject.getInt("tableCapacity"));
                tableItemDto.setTableStatus(jsonObject.getString("tableStatus"));
                // inserting the entity to the datastore.
                tableItemDto = this.tableService.insertTable(tableItemDto);
            }
            else {
                for (int i = 0; i < validator.getErrors().size(); i++) {
                    tableItemDto.getErrorList().add(validator.getErrors().get(i));
                }
            }
            if(tableItemDto.getErrorList().size() > 0) {
                jsonObject.put("errorList", tableItemDto.getErrorList());
            }
        }catch(Exception e) {
            // Adds an error message if there exists.
            tableItemDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == jsonObject) {
                jsonObject = new JSONObject();
            }
        }
        
        // add error messages to the json object.
        jsonObject.put("errorList", tableItemDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(jsonObject.toString());
        
        // no screen redirection.
        return null;
    }
}
