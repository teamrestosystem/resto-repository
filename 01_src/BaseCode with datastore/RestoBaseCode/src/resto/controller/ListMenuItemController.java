/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import resto.common.GlobalConstants;
import resto.dto.MenuItemListDto;
import resto.service.MenuItemService;

/**
 * Controller used to retrieve all 'Menu Item' from the datastore.
 * @author Team Rocket
 * @version 0.02
 * Version History
 * [09/21/2016] 0.01 � Keith Edmond L. Araneta � Initial codes
 * [09/28/2016] 0.01 � Keith Edmond L. Araneta � added type of response
 */

public class ListMenuItemController extends Controller {
    
    private MenuItemService menuItemService = new MenuItemService();

    @Override
    protected Navigation run() throws Exception {
        /**
         * Use to store the information that will be passed to the service.
         */
        MenuItemListDto menuItemListDto = new MenuItemListDto();
        /**
         * Holds the information to be passed in the response.
         */
        JSONObject jsonObject = null;
        
        try {
            jsonObject = new JSONObject();
            // getting the list of items
            menuItemListDto = menuItemService.getMenuItemList();
            // adding the list to the json that will be passed as response.
            jsonObject.put("menuItemList", menuItemListDto.getEntries());
        } catch (Exception e) {
            // add error message
            menuItemListDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == jsonObject) {
                jsonObject = new JSONObject();
            }
        }
        
        // add error messages to the json object.
        jsonObject.put("errorList", menuItemListDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(jsonObject.toString());
        
        return null;
    }
    
}
