
/* Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import resto.common.GlobalConstants;
import resto.dto.MenuItemDto;
import resto.service.MenuItemService;
import resto.utils.JSONValidators;

/**
 * Controller used to insert a 'Menu Item' to the datastore.
 * @author Team Rocket
 * @version 0.02
 * Version History
 * [08/31/2016] 0.01 � Teresita Tala P. Rabago � Initial codes.
 * [09/28/2016] 0.02 � Keith Edmond L. Araneta � Updated run() function ; JSON implementation
 */

public class UpdateMenuItemController extends Controller{
    
    /**
    * Service object that will be used to call the insert function to datastore.
    */
    MenuItemService menuItemService = new MenuItemService();
    
    /**
     * The function that will be ran by default when this class is called.
     */
    @Override
    public Navigation run() throws Exception {
        /**
         * Used to store the information from the request and send to the
         * service class.
         */
        MenuItemDto menuItemDto = new MenuItemDto();
        /**
         * Holds the data sent from the request of the client side.
         */
        JSONObject jsonObject= null;
        
        try {
            jsonObject = new JSONObject(this.request.getReader().readLine());
            JSONValidators validator = new JSONValidators(jsonObject);
            
            validator.add("id", validator.required());
            validator.add("menuItemName", validator.required());
            validator.add("servings", validator.required());
            validator.add("price", validator.required());
            validator.add("status", validator.required());
            validator.add("category", validator.required());
            validator.add("imagePath", validator.required());
            
            if(validator.validate()) {
                // add into the datastore
                
                // Getting all the information sent from the request.
                menuItemDto.setId(jsonObject.getLong("id"));
                menuItemDto.setMenuItemName(jsonObject.getString("menuItemName"));
                menuItemDto.setServings(jsonObject.getInt("servings"));
                menuItemDto.setPrice(jsonObject.getDouble("price"));
                menuItemDto.setStatus(jsonObject.getString("status"));
                menuItemDto.setCategory(jsonObject.getString("category"));
                menuItemDto.setImagePath(jsonObject.getString("imagePath"));
                
                menuItemDto = this.menuItemService.updateMenuItem(menuItemDto);
            }
            else {
                for (int i = 0; i < validator.getErrors().size(); i++) {
                    menuItemDto.getErrorList().add(validator.getErrors().get(i));
                }
            }
            if(menuItemDto.getErrorList().size() > 0) {
                jsonObject.put("errorList", menuItemDto.getErrorList());
            }
        }catch(Exception e) {
            // Adds an error message if there exists.
            menuItemDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == jsonObject) {
                jsonObject = new JSONObject();
            }
        }
        
        // add error messages to the json object.
        jsonObject.put("errorList", menuItemDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(jsonObject.toString());
        
        // no screen redirection.
        return null;
    }
}
