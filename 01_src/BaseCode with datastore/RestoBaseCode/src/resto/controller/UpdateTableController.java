
/* Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import resto.common.GlobalConstants;
import resto.dto.TableDto;
import resto.service.TableService;
import resto.utils.JSONValidators;

/**
 * Controller used to update a 'table status' to the datastore.
 * @author Team Rocket
 * @version 0.02
 * Version History
 * [10/16/2016] 0.01 � Loregge M. Kandt - initial codes
 */


public class UpdateTableController extends Controller{
    
    /**
    * Service object that will be used to call the insert function to datastore.
    */
    TableService tableService = new TableService();
    
    /**
     * The function that will be ran by default when this class is called.
     */
    @Override
    public Navigation run() throws Exception {
        System.out.println("UpdateTableController.run " + "start");
        /**
         * Used to store the information from the request and send to the
         * service class.
         */

        TableDto tableDto = new TableDto();
        /**
         * Holds the data sent from the request of the client side.
         */
        JSONObject jsonObject= null;
        
        try {
            jsonObject = new JSONObject(this.request.getReader().readLine());
            JSONValidators validator = new JSONValidators(jsonObject);
            
            validator.add("id", validator.required());
            validator.add("tableNumber", validator.required());
            validator.add("tableCapacity", validator.required());
            validator.add("tableStatus", validator.required());
            
            if(validator.validate()) {
                // add into the datastore
                
                // Getting all the information sent from the request.
                tableDto.setId(jsonObject.getLong("id"));
                tableDto.setTableNumber(jsonObject.getString("tableNumber"));
                tableDto.setTableCapacity(jsonObject.getInt("tableCapacity"));
                tableDto.setTableStatus(jsonObject.getString("tableStatus"));
                tableDto = this.tableService.updateTable(tableDto);
            }
            else {
                for (int i = 0; i < validator.getErrors().size(); i++) {
                    tableDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }
            if(tableDto.getErrorList().size() > 0) {
                jsonObject.put("errorList", tableDto.getErrorList());
            }
        }catch(Exception e) {
            System.out.println(e.toString());
            // Adds an error message if there exists.
            tableDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == jsonObject) {
                jsonObject = new JSONObject();
            }
        }
        
        // add error messages to the json object.
        jsonObject.put("errorList", tableDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(jsonObject.toString());
        
        // no screen redirection.
        return null;
    }
}

