/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import resto.common.GlobalConstants;
import resto.dto.MenuItemListDto;
import resto.dto.OrderListDto;
import resto.service.MenuItemService;
import resto.service.OrderService;

/**
 * Controller used to retrieve all 'Menu Item' from the datastore.
 * @author Team Rocket
 * @version 0.02
 * Version History
 * [09/26/2016] 0.01 � Teresita Tala P. Rabago � Initial codes
 * [09/29/2016] 0.02 � Teresita Tala P. Rabago � Updated some codes
 */

public class ListOrderController extends Controller{

    private OrderService orderItemService = new OrderService();
    
    @Override
    protected Navigation run() throws Exception {
        
        /**
         * Use to store the information that will be passed to the service.
         */
        OrderListDto orderListDto = new OrderListDto();
        /**
         * Holds the information to be passed in the response.
         */
        OrderService orderService = new OrderService();

        JSONObject jsonObject = null;
        
        try {
            jsonObject = new JSONObject();
            // getting the list of items
            orderListDto = orderService.getOrderItemList();
            // adding the list to the json that will be passed as response.
            jsonObject.put("orderList", orderListDto.getEntries());
        } catch (Exception e) {
            // add error message
            orderListDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == jsonObject) {
                jsonObject = new JSONObject();
            }
        }
        
        // add error messages to the json object.
        jsonObject.put("errorList", orderListDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(jsonObject.toString());
        
        return null;
    }

}
