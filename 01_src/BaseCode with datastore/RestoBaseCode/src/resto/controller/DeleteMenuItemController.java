/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
package resto.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import resto.common.GlobalConstants;
import resto.dto.MenuItemDto;
import resto.service.MenuItemService;

/**
 * Controller used to delete a 'Menu Item' from the datastore.
 * @author Team Rocket
 * @version 0.01
 * Version History
 * [08/31/2016] 0.01 - Keith Edmond L. Araneta - Initial codes.
 */
public class DeleteMenuItemController extends Controller {
    
    /**
     * Service object that will be used to call the delete function to datastore.
     */
    MenuItemService menuItemService = new MenuItemService();

    /**
     * The function that will be ran first upon entering this controller.
     * Used to delete a 'Menu Item' entity from the datastore.
     */
    @Override
    protected Navigation run() throws Exception {
        /**
         * Used to store the information from the request and send to the
         * service class.
         */
        MenuItemDto menuItemDto = new MenuItemDto();
        JSONObject jsonObject = null;
        
        try {
            jsonObject = new JSONObject(this.request.getReader().readLine());

            menuItemDto.setId(jsonObject.getLong("id"));

            menuItemDto = menuItemService.deleteMenuItem(menuItemDto);
        } catch (Exception e) {
            // Adds an error message if there exists.
            menuItemDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == jsonObject) {
                jsonObject = new JSONObject();
            }
        }
        
        // add error messages to the json object.
        jsonObject.put("errorList", menuItemDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(jsonObject.toString());
        
        // no screen redirection.
        return null;
    }
    
}
