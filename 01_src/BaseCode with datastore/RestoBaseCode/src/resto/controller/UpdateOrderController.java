package resto.controller;

import java.util.ArrayList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import resto.common.GlobalConstants;
import resto.dto.MenuItemDto;
import resto.dto.OrderDto;
import resto.service.MenuItemService;
import resto.service.OrderService;
import resto.utils.JSONValidators;

/**
* Controller used to insert an 'Order' to the datastore.
* @author Team Rocket
* @version 0.02
* Version History
* [10/17/2016] 0.01 � Keith � Initial codes.
 */

public class UpdateOrderController extends Controller {
    /**
    * Service object that will be used to call the insert function to datastore.
    */
    OrderService orderService = new OrderService();
        
    /**
    * The function that will be ran first upon entering this controller.
    * Used to insert an 'Order' entity to the datastore.
    */
    @Override
    public Navigation run() throws Exception {
        /**
        * Used to store the information from the request and send to the
        * service class.
        */
        MenuItemDto menuItemDto = null;
        OrderDto orderDto = new OrderDto();
        /**
        * Holds  the data sent from the request of the client side
        * */
        JSONObject jsonObject = null;
            
        try {
                
            jsonObject = new JSONObject(this.request.getReader().readLine());                  
            JSONValidators validator = new JSONValidators(jsonObject);
            
            validator.add("id", validator.required());
            validator.add("orderNumber", validator.required());
            validator.add("tableNumber", validator.required());
            validator.add("orderTimestamp",validator.required());
            validator.add("menuItems",validator.required());
            validator.add("totalCost",validator.required());
            validator.add("status",validator.required());
                 
            if(validator.validate()){
                     
                JSONArray menuItemArray = jsonObject.getJSONArray("menuItems");
                List<MenuItemDto> menuItemDtoList = new ArrayList<MenuItemDto>();
                     
                for(int i = 0 ; i < menuItemArray.length(); i++) {
                    menuItemDto = new MenuItemDto();
                    menuItemDto.setMenuItemName(menuItemArray.getJSONObject(i).getString("menuItemName"));
                    menuItemDto.setServings(menuItemArray.getJSONObject(i).getInt("servings"));
                    menuItemDto.setPrice(menuItemArray.getJSONObject(i).getDouble("price"));
                    menuItemDto.setStatus(menuItemArray.getJSONObject(i).getString("status"));
                    menuItemDto.setCategory(menuItemArray.getJSONObject(i).getString("category"));
                    menuItemDto.setImagePath(menuItemArray.getJSONObject(i).getString("imagePath"));
                    menuItemDtoList.add(menuItemDto);
                }
                
                orderDto.setId(jsonObject.getLong("id"));
                orderDto.setOrderNumber(jsonObject.getLong("orderNumber"));
                orderDto.setTableNumber(jsonObject.getString("tableNumber"));
                orderDto.setOrderTimestamp(jsonObject.getString("orderTimestamp"));
                orderDto.setMenuItems(menuItemDtoList);
                orderDto.setStatus(jsonObject.getString("status"));
                orderDto.setTotalCost(jsonObject.getDouble("totalCost"));
                
                orderDto = this.orderService.updateOrder(orderDto);
            }
            else {
                for(int i=0; i<validator.getErrors().size(); i++){
                    orderDto.getErrorList().add(validator.getErrors().get(i));
                }
            }
                 
           if(orderDto.getErrorList().size() > 0){
               jsonObject.put("errorList", orderDto.getErrorList());
           }
                 
                
       } catch (Exception e) {
           //initialize a JSON object that will be passed as a response                
           if(jsonObject == null){
               jsonObject = new JSONObject();
           }                
           //adding of error messages if there's any
           orderDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
       }
            
      //adding of error messages to the JSON object
      jsonObject.put("errorList", orderDto.getErrorList());
            
       //setting of the type of response
       response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
       //send the response back to JS
       response.getWriter().write(jsonObject.toString());
            
        //no screen redirection.
       return null;
    }
}
