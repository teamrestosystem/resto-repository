/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */

package resto.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import resto.common.GlobalConstants;
import resto.dto.MenuItemListDto;
import resto.dto.TableListDto;
import resto.service.MenuItemService;
import resto.service.TableService;

/**
 * Controller used to retrieve all 'Menu Item' from the datastore.
 * @author Team Rocket
 * @version 0.02
 * Version History
 * [09/26/2016] 0.01 � Teresita Tala P. Rabago � Initial codes
 * [09/28/2016] 0.02 � Teresita Tala P. Rabago � some codes
 * [09/29/2016] 0.02 � Teresita Tala P. Rabago � Updated some codes
 */

public class ListTableController extends Controller{

    private TableService tableItemService = new TableService();
    
    @Override
    protected Navigation run() throws Exception {
        
        /**
         * Use to store the information that will be passed to the service.
         */
        TableListDto tableListDto = new TableListDto();
        /**
         * Holds the information to be passed in the response.
         */
        JSONObject jsonObject = null;
        
        try {
            jsonObject = new JSONObject();
            // getting the list of items
            tableListDto = tableItemService.getTableItemList();
            // adding the list to the json that will be passed as response.
            jsonObject.put("tableList", tableListDto.getEntries());
        } catch (Exception e) {
            // add error message
            tableListDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == jsonObject) {
                jsonObject = new JSONObject();
            }
        }
        
        // add error messages to the json object.
        jsonObject.put("errorList", tableListDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to JS.
        response.getWriter().write(jsonObject.toString());
        
        return null;
    }

}
