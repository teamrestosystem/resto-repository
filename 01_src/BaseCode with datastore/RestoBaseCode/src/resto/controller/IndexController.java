package resto.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

public class IndexController extends Controller {

    //TestService service = new TestService();

    @Override
    public Navigation run() throws Exception {
        // screen redirection
        return forward("/html/customer_screen.html");
    }
}
