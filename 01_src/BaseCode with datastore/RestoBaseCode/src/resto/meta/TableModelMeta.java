package resto.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2016-10-17 01:25:41")

/** */
public final class TableModelMeta extends org.slim3.datastore.ModelMeta<resto.model.TableModel> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<resto.model.TableModel, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<resto.model.TableModel, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<resto.model.TableModel, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<resto.model.TableModel, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<resto.model.TableModel, java.lang.Integer> tableCapacity = new org.slim3.datastore.CoreAttributeMeta<resto.model.TableModel, java.lang.Integer>(this, "tableCapacity", "tableCapacity", int.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<resto.model.TableModel> tableNumber = new org.slim3.datastore.StringAttributeMeta<resto.model.TableModel>(this, "tableNumber", "tableNumber");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<resto.model.TableModel> tableStatus = new org.slim3.datastore.StringAttributeMeta<resto.model.TableModel>(this, "tableStatus", "tableStatus");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<resto.model.TableModel, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<resto.model.TableModel, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final TableModelMeta slim3_singleton = new TableModelMeta();

    /**
     * @return the singleton
     */
    public static TableModelMeta get() {
       return slim3_singleton;
    }

    /** */
    public TableModelMeta() {
        super("TableModel", resto.model.TableModel.class);
    }

    @Override
    public resto.model.TableModel entityToModel(com.google.appengine.api.datastore.Entity entity) {
        resto.model.TableModel model = new resto.model.TableModel();
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setKey(entity.getKey());
        model.setTableCapacity(longToPrimitiveInt((java.lang.Long) entity.getProperty("tableCapacity")));
        model.setTableNumber((java.lang.String) entity.getProperty("tableNumber"));
        model.setTableStatus((java.lang.String) entity.getProperty("tableStatus"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        resto.model.TableModel m = (resto.model.TableModel) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("id", m.getId());
        entity.setProperty("tableCapacity", m.getTableCapacity());
        entity.setProperty("tableNumber", m.getTableNumber());
        entity.setProperty("tableStatus", m.getTableStatus());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        resto.model.TableModel m = (resto.model.TableModel) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        resto.model.TableModel m = (resto.model.TableModel) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        resto.model.TableModel m = (resto.model.TableModel) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        resto.model.TableModel m = (resto.model.TableModel) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        resto.model.TableModel m = (resto.model.TableModel) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        writer.setNextPropertyName("tableCapacity");
        encoder0.encode(writer, m.getTableCapacity());
        if(m.getTableNumber() != null){
            writer.setNextPropertyName("tableNumber");
            encoder0.encode(writer, m.getTableNumber());
        }
        if(m.getTableStatus() != null){
            writer.setNextPropertyName("tableStatus");
            encoder0.encode(writer, m.getTableStatus());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected resto.model.TableModel jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        resto.model.TableModel m = new resto.model.TableModel();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("tableCapacity");
        m.setTableCapacity(decoder0.decode(reader, m.getTableCapacity()));
        reader = rootReader.newObjectReader("tableNumber");
        m.setTableNumber(decoder0.decode(reader, m.getTableNumber()));
        reader = rootReader.newObjectReader("tableStatus");
        m.setTableStatus(decoder0.decode(reader, m.getTableStatus()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}