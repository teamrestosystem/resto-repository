package resto.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2016-10-17 01:25:42")

/** */
public final class MenuItemModelMeta extends org.slim3.datastore.ModelMeta<resto.model.MenuItemModel> {

    /** */
    public final org.slim3.datastore.StringAttributeMeta<resto.model.MenuItemModel> category = new org.slim3.datastore.StringAttributeMeta<resto.model.MenuItemModel>(this, "category", "category");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<resto.model.MenuItemModel, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<resto.model.MenuItemModel, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<resto.model.MenuItemModel> imagePath = new org.slim3.datastore.StringAttributeMeta<resto.model.MenuItemModel>(this, "imagePath", "imagePath");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<resto.model.MenuItemModel, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<resto.model.MenuItemModel, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<resto.model.MenuItemModel> menuItemName = new org.slim3.datastore.StringAttributeMeta<resto.model.MenuItemModel>(this, "menuItemName", "menuItemName");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<resto.model.MenuItemModel, java.lang.Double> price = new org.slim3.datastore.CoreAttributeMeta<resto.model.MenuItemModel, java.lang.Double>(this, "price", "price", double.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<resto.model.MenuItemModel, java.lang.Integer> servings = new org.slim3.datastore.CoreAttributeMeta<resto.model.MenuItemModel, java.lang.Integer>(this, "servings", "servings", int.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<resto.model.MenuItemModel> status = new org.slim3.datastore.StringAttributeMeta<resto.model.MenuItemModel>(this, "status", "status");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<resto.model.MenuItemModel, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<resto.model.MenuItemModel, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final MenuItemModelMeta slim3_singleton = new MenuItemModelMeta();

    /**
     * @return the singleton
     */
    public static MenuItemModelMeta get() {
       return slim3_singleton;
    }

    /** */
    public MenuItemModelMeta() {
        super("MenuItemModel", resto.model.MenuItemModel.class);
    }

    @Override
    public resto.model.MenuItemModel entityToModel(com.google.appengine.api.datastore.Entity entity) {
        resto.model.MenuItemModel model = new resto.model.MenuItemModel();
        model.setCategory((java.lang.String) entity.getProperty("category"));
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setImagePath((java.lang.String) entity.getProperty("imagePath"));
        model.setKey(entity.getKey());
        model.setMenuItemName((java.lang.String) entity.getProperty("menuItemName"));
        model.setPrice(doubleToPrimitiveDouble((java.lang.Double) entity.getProperty("price")));
        model.setServings(longToPrimitiveInt((java.lang.Long) entity.getProperty("servings")));
        model.setStatus((java.lang.String) entity.getProperty("status"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        resto.model.MenuItemModel m = (resto.model.MenuItemModel) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("category", m.getCategory());
        entity.setProperty("id", m.getId());
        entity.setProperty("imagePath", m.getImagePath());
        entity.setProperty("menuItemName", m.getMenuItemName());
        entity.setProperty("price", m.getPrice());
        entity.setProperty("servings", m.getServings());
        entity.setProperty("status", m.getStatus());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        resto.model.MenuItemModel m = (resto.model.MenuItemModel) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        resto.model.MenuItemModel m = (resto.model.MenuItemModel) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        resto.model.MenuItemModel m = (resto.model.MenuItemModel) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        resto.model.MenuItemModel m = (resto.model.MenuItemModel) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        resto.model.MenuItemModel m = (resto.model.MenuItemModel) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getCategory() != null){
            writer.setNextPropertyName("category");
            encoder0.encode(writer, m.getCategory());
        }
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getImagePath() != null){
            writer.setNextPropertyName("imagePath");
            encoder0.encode(writer, m.getImagePath());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getMenuItemName() != null){
            writer.setNextPropertyName("menuItemName");
            encoder0.encode(writer, m.getMenuItemName());
        }
        writer.setNextPropertyName("price");
        encoder0.encode(writer, m.getPrice());
        writer.setNextPropertyName("servings");
        encoder0.encode(writer, m.getServings());
        if(m.getStatus() != null){
            writer.setNextPropertyName("status");
            encoder0.encode(writer, m.getStatus());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected resto.model.MenuItemModel jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        resto.model.MenuItemModel m = new resto.model.MenuItemModel();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("category");
        m.setCategory(decoder0.decode(reader, m.getCategory()));
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("imagePath");
        m.setImagePath(decoder0.decode(reader, m.getImagePath()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("menuItemName");
        m.setMenuItemName(decoder0.decode(reader, m.getMenuItemName()));
        reader = rootReader.newObjectReader("price");
        m.setPrice(decoder0.decode(reader, m.getPrice()));
        reader = rootReader.newObjectReader("servings");
        m.setServings(decoder0.decode(reader, m.getServings()));
        reader = rootReader.newObjectReader("status");
        m.setStatus(decoder0.decode(reader, m.getStatus()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}