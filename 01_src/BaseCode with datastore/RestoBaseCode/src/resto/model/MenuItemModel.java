/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
package resto.model;

import java.io.Serializable;

import com.google.appengine.api.datastore.Key;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.Model;

/**
 * Contains the properties and methods for the 'Menu Item' entity.
 * @author Team Rocket
 * @version 0.02
 * Version History
 * [08/10/2016] 0.01 � Loregge M. Kandt  � Initial codes.
 * [08/10/2016] 0.02 � Keith Edmond L. Araneta  - Added the properties(variables) of the 
 *                                                "Menu Item" entity; getters and setters.
 * [09/21/2016] 0.02 � Keith Edmond L. Araneta - Added property 'category' of "Menu Item"
 * [10/05/2016] 0.02 � Keith Edmond L. Araneta - Added property 'imagePath' of "Menu Item"                                          
 */

@Model(schemaVersion = 1)
public class MenuItemModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Attribute(primaryKey = true)
    private Key key;

    @Attribute(version = true)
    private Long version;
    
    /**
     * The id of the 'Menu Item'.
     */
    private Long id;
    
    /**
     * The name of the 'Menu Item'
     */
    private String menuItemName;
    
    /**
     * The number of available servings of the 'Menu Item'
     */
    private int servings;

    
    /**
     * The price per serving of the 'Menu Item'
     */
    private double price;
    
    /**
     * The status of the 'Menu Item'; (available or unavailable)
     */
    private String status;
    
    /**
     * The category of the 'Menu Item';
     */
    private String category;
    
    /**
     * The path of the image of the 'Menu Item';
     */
    private String imagePath;
    
    /**
     * Retrieves the id of the 'Menu Item'.
     * @return the id of the 'Menu Item'.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id of the 'Menu Item'.
     * @param id - the value to be set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Retrieves the name of the 'Menu Item'.
     * @return the name of the 'Menu Item'.
     */
    public String getMenuItemName() {
        return menuItemName;
    }

    /**
     * Sets the name of the 'Menu Item'
     * @param menuItemName - the value to be set
     */
    public void setMenuItemName(String menuItemName) {
        this.menuItemName = menuItemName;
    }

    /**
     * Retrieves the number of available servings of the 'Menu Item'.
     * @return the number of available servings of the 'Menu Item'.
     */
    public int getServings() {
        return servings;
    }

    /**
     * Sets the number of available servings of the 'Menu Item'.
     * @param servings - the value to be set
     */
    public void setServings(int servings) {
        this.servings = servings;
    }

    /**
     * Retrieves the price of the 'Menu Item'.
     * @return the price of the 'Menu Item'.
     */
    public double getPrice() {
        return price;
    }

    /**
     * Sets the price of the 'Menu Item'.
     * @param price - the value to be set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Retrieves the status of the 'Menu Item'; (available or unavailable).
     * @return the status of the 'Menu Item'.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the status of the 'Menu Item'; (available or unavailable).
     * @param status - the value to be set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
    /**
     * Retrieves the category of the 'Menu Item'
     * @return the category of the 'Menu Item'.
     */
    public String getCategory() {
        return category;
    }
    
    /**
     * Sets the category of the 'Menu Item'
     * @param category - the value to be set
     */
    public void setCategory(String category) {
        this.category = category;
    }
    
    /**
     * Retrieves the path of the image of the 'Menu Item'
     * @return the path of the image of the 'Menu Item'.
     */
    public String getImagePath() {
        return imagePath;
    }
    
    /**
     * Sets the path of the image of the 'Menu Item'
     * @param imagePath - the value to be set
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    /**
     * Returns the key.
     *
     * @return the key
     */
    public Key getKey() {
        return key;
    }

    /**
     * Sets the key.
     *
     * @param key
     *            the key
     */
    public void setKey(Key key) {
        this.key = key;
    }

    /**
     * Returns the version.
     *
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MenuItemModel other = (MenuItemModel) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        return true;
    }
}
