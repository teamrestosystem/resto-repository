/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
package resto.model;

import java.io.Serializable;

import com.google.appengine.api.datastore.Key;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.Model;

/**
 * Contains the properties and methods for the 'Menu Item' entity.
 * @author Team Rocket
 * @version 0.02
 * Version History
 * [09/29/2016] 0.01 � Teresita Tala P. Rabago  � Initial codes.
 */

@Model(schemaVersion = 1)
public class TableModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Attribute(primaryKey = true)
    private Key key;

    @Attribute(version = true)
    private Long version;
    
    /**
     * The id of the 'table'.
     */
    private Long id;
    
    /**
     * The number of the 'table'.
     */
    private String tableNumber;
    
    /**
     * The capacity of the 'table'.
     */
    private int tableCapacity;
    
    /**
     * The status of the 'table'.
     */
    private String tableStatus;
    
    /**
     * Retrieves the id of the 'table'.
     * @return the id of the 'table'.
     */
    public Long getId() {
        return id;
    }
    
    /**
     * Sets the id of the 'table'
     * @param id - the value to be set
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * Retrives the number of the 'table'.
     * @return the number of the 'table'.
     */
    public String getTableNumber() {
        return tableNumber;
    }
    
    /**
     * Sets the number of the 'table'.
     * @param tableNo - the value to be set
     */
    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }
    
    /**
     * Retrieves the capacity of the 'table'
     * @return the capacity of the 'table'
     */
    public int getTableCapacity() {
        return tableCapacity;
    }
    
    /**
     * Sets the capacity of the 'table'
     * @param tableCapacity - the value to be set
     */
    public void setTableCapacity(int tableCapacity) {
        this.tableCapacity = tableCapacity;
    }
    
    /**
     * Retrieves the status of the 'table'.
     * @return the status of the 'table'.
     */
    public String getTableStatus() {
        return tableStatus;
    }
    
    /**
     * Sets the status of the 'table'.
     * @param tableStatus - the value to be set.
     */
    public void setTableStatus(String tableStatus) {
        this.tableStatus = tableStatus;
    } 
    /**
     * Returns the key.
     *
     * @return the key
     */
    public Key getKey() {
        return key;
    }

    /**
     * Sets the key.
     *
     * @param key
     *            the key
     */
    public void setKey(Key key) {
        this.key = key;
    }

    /**
     * Returns the version.
     *
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TableModel other = (TableModel) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        return true;
    }
}
