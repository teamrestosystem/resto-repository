package resto.model;

import java.io.Serializable;
import java.util.List;

import com.google.appengine.api.datastore.Key;
//import com.google.appengine.repackaged.com.google.protobuf.proto1api.Timestamp;

import resto.dto.MenuItemDto;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.Model;

/**
 * Contains the properties and methods for the 'Order' entity.
 * @author Team Rocket
 * @version 0.01
 * Version History
 * [09/21/2016] 0.01 � Ryan L. Lobitana
 * [10/16/2016] 0.02 - Keith Edmond L. Araneta - added property 'tableId'
 */

@Model(schemaVersion = 1)
public class OrderModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Attribute(primaryKey = true)
    private Key key;

    @Attribute(version = true)
    private Long version;
    
    /**
     * The id of the 'Menu Item'.
     */
    private Long id;
        
    /**
     * Order�s number (can be referred to as receipt number)
     */    
    private Long orderNumber;
    
    /**
     * The id of the 'Table'
     */    
    private String tableNumber;
    
    /**
     * Date and time when the order was taken
     */
    private String orderTimestamp;    

    /**
     * The list of ordered menu items
     */
    @Attribute(lob = true)
    private List<MenuItemDto> menuItems;
    
    /**
     * The total cost of all the ordered items
     */
    private double totalCost;
    
    /**
     * The status of the ordered items
     */
    private String status;                
    
    /**
     * Returns the key.
     *
     * @return the key
     */
    public Key getKey() {
        return key;
    }
       
    /**
     * Sets the key.
     *
     * @param key
     *            the key
     */
    public void setKey(Key key) {
        this.key = key;
    }

    /**
     * Returns the version.
     *
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OrderModel other = (OrderModel) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        return true;
    }
        
    public Long getOrderNumber() {
        return orderNumber;
    }
    
    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<MenuItemDto> getMenuItems() {
        return menuItems;
    }
    

    public void setMenuItems(List<MenuItemDto> menuItems) {
        this.menuItems = menuItems;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getOrderTimestamp() {
        return orderTimestamp;
    }

    public void setOrderTimestamp(String orderTimestamp) {
        this.orderTimestamp = orderTimestamp;
    }
    
    public String getTableNumber() {
        return tableNumber;
    }
    
    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }
}
