/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
 
/*
* This file contains the backend process for the 'Admin Order' screen
* that is under the 'orderController'.
* The holds the values/properties that is common to the application.
* @author Team Rocket
 * @version 0.01
 * Version History
 * [09/07/2016] 0.01 – Keith Edmond L. Araneta  – Initial codes.
 * [09/11/2016] 0.02 � Teresita Tala P. Rabago  � some Codes.
 * [09/21/2016] 0.02 ï¿½ Teresita Tala P. Rabago  ï¿½ filteringSearchOrder function.
 * [09/29/2016] 0.02 ï¿½ Teresita Tala P. Rabago  ï¿½ implemented listOrders().
 * [10/08/2016] 0.02 ï¿½ Teresita Tala P. Rabago  ï¿½ updated some codes and Implemented deleteOrder(orderItemId).
 * [10/17/2016] 0.02 ï¿½ Teresita Tala P. Rabago  Added some Codes
 */

app.controller('orderController', ['$scope', '$http', '$rootScope', 'ngDialog', '$filter', function ($scope, $http, $rootScope, ngDialog, $filter) {

	/**
     * Holds the array of orders that will be used to populate 
     * the table in admin_screen_order.html
     */
	$scope.orderList = [];
	
	$scope.filteredArrayOrder = null;	
	
	$scope.filteringSearchOrder = function(){
		if($scope.searchTextOrder != null){
			var filtered = [];
		    for (var i = 0; i < $scope.orderList.length; i++) {
		      if($scope.orderList[i].status.toLowerCase().indexOf($scope.searchTextOrder.toLowerCase()) !== -1 && 
		    	 $scope.orderList[i].status.toLowerCase().indexOf($scope.searchTextOrder.toLowerCase()) === 0) {
		        filtered.push($scope.orderList[i]);
		      }
		    }
		    $scope.filteredArrayOrder = filtered;
		}
		else
			$scope.filteredArrayOrder = $scope.orderList;
	};
	
	/**
	 * Used to retrieve all order items from the datastore
	 */
	$scope.listOrders = function() {
		$http.get("/ListOrder")
			.then(function(response) {
				if (response.data.errorList.length == 0) {
					$scope.orderList = response.data.orderList;
					$scope.filteredArrayOrder = $scope.orderList;
				} else {
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i] += "\n";
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured");
			});
	}
	
	//di pa ni mao
	$scope.listMenuItems = function(menuItems) {
		var temp = $scope.MenuOrderList(menuItems);
		var str = "";
		for(var i = 0; i < temp.length; i++) {
			if(i==0)
				str += temp[i].menuName + "(" + temp[i].menuQuantity + ")";
			else
				str += ", " + temp[i].menuName + "(" + temp[i].menuQuantity + ")";
		}
		return str;
	}
	
	/**
	 * Used to delete a 'orderItem'
	 */
	$scope.deleteOrder = function(orderItemId){
		var confirmation = window.confirm("Are you sure you want to delete order?");
		if (true == confirmation) {
			/* creating an object that will contain the information for deleting a order item
			*/
			var order = {
				id: orderItemId
			}
			
			$http.post("/DeleteOrderItem", order)
				.then(function(response) {
					if (response.data.errorList.length == 0) {
						alert("Successfully Deleted Order");
						$scope.listOrders();
					} else {
						var errorMessage = "";
						for (var i = 0; i < response.data.errorList.length; i++) {
							errorMessage += response.data.errorList[i] += "\n";
						}
						alert(errorMessage);
					}
				}, function() {
					alert("An error has occured");
				});
		}
	}
	
	/**
	 * Used to get the quantity of each order of menu item
	 */
	$scope.MenuOrderList = function(menuItems){
		var temp = [];
		var menuOrder = {
			menuName: menuItems[0].menuItemName,
			price: menuItems[0].price,
			menuQuantity: 1
		};
		temp.push(menuOrder);
		
		for(var i=1; i<menuItems.length; i++){
			for(var j=0; j<temp.length; j++){
				if(temp[j].menuName === menuItems[i].menuItemName){
					temp[j].menuQuantity++;
					break;
				}
				else if( j == temp.length-1){
					var menuOrder = {
							menuName: menuItems[i].menuItemName,
							price: menuItems[i].price,
							menuQuantity: 1
					};
					temp.push(menuOrder);
					break;
				}
			}
		}		
		return temp;
	}
	
	/**
	 * Used to open the modal containing the form for menu orders
	 */
	$scope.openModal = function(index){
		console.log("admin_order.orderController.openModal " + "start");
		
		//$scope.MenuOrderQuantity($scope.filteredArrayOrder[index].menuItems);
		$scope.menuOrderList = $scope.MenuOrderList($scope.filteredArrayOrder[index].menuItems);
		
		$scope.menuItemsList = 
		ngDialog.open({
			template: '/html/templates/admin_screen_order_menuOrder.html',
			scope: $scope,

			controller: 'orderController',

			preCloseCallback:function(){ 
				$scope.addUpdate = "ADD";
				$scope.updateId = null;
			}
		});
		
		console.log("admin_order.orderController.openModal " + "end");
	}
	
	$scope.listOrders();
}]);
