/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities and global variables for the application.
* @author Team Rocket
* @version 0.01
* Version History
* [10/09/2016] 0.01 Loregge M. Kandt added disable back button
**/



app.controller('adminDisableBackButton', function($scope) {

	/**
	 * Author: Wael Sidawi
	 * Event-Listner for Back-Button
	 */
	$scope.$on('$locationChangeStart', function(event, next, current){            
	    // Prevent the browser default action (Going back):
	    event.preventDefault();  
	});
});