/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
 
/*
* This file contains the backend process for the 'Admin Menu' screen
* that is under the 'menuController' and 'adminModalController'
* The holds the values/properties that is common to the application.
* @author Team Rocket
 * @version 0.01
 * Version History
 * [09/07/2016] 0.01 – Keith Edmond L. Araneta  – Initial codes.
 * [09/11/2016] 0.01 – Keith Edmond L. Araneta  – Added register menu functionality(static) and some codes.
 * [09/21/2016] 0.02 - Keith Edmond L. Araneta  - Updated registerMenuItem() function;can add to dataStore using JSON and AJAX
 * [09/28/2016] 0.02 - Keith Edmond L. Araneta  - Added and implemented updateMenuItem() function
 * [10/05/2016] 0.02 - Keith Edmond L. Araneta  - Updates for 'menuItem' images
 * [10/11/2016] 0.02 - Keith Edmond L. Araneta  - Added and implemented functions canDelete() and canUpdate()
 */

app.controller('menuController', ['$scope', '$http', '$rootScope', 'ngDialog', function ($scope, $http, $rootScope, ngDialog)  {
	
	$scope.addUpdate = "ADD";
	$scope.updateId = null;
	
	/**
     * Holds the array of orders that will be used to populate 
     * the table in admin_screen_order.html
     */
	$scope.orderList = [];
	
	/**
	 * Used to open the modal containing the form for adding a 'menuItem'
	 */
	$scope.openModal = function(){
		ngDialog.open({
			template: '/html/templates/admin_screen_menu_createMenuModal.html',
			scope: $scope,
			controller: 'adminModalController',
			preCloseCallback:function(){ 
				$scope.addUpdate = "ADD";
				$scope.updateId = null;
			}
		});
	}
	
	/**
	 * Used to retrieve all menu items from the datastore
	 */
	$scope.listMenuItems = function() {
		$http.get("/ListMenuItem")
			.then(function(response) {
				if (response.data.errorList.length == 0) {
					$scope.$parent.menuItemList = response.data.menuItemList;
					$scope.menuItemListHolder = $scope.$parent.menuItemList;
				} else {
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i] += "\n";
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured");
			});
	}
	
	/**
	 * Used to retrieve all order items from the datastore
	 */
	$scope.listOrderItems = function() {
		$http.get("/ListOrder")
			.then(function(response) {
				if (response.data.errorList.length == 0) {
					$scope.orderList = response.data.orderList;
				} else {
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i] += "\n";
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured");
			});
	}
	
	/**
	 * Used to filter the content of the table holding the menu items during search
	 */
	$scope.filterTableContent = function() {
		if($scope.searchInput != null) {
			$scope.menuItemListHolder = [];
			for(var i = 0; i < $scope.$parent.menuItemList.length; i++) {
				if($scope.$parent.menuItemList[i].menuItemName.toLowerCase().indexOf($scope.searchInput.toLowerCase()) != -1 ||
				   $scope.$parent.menuItemList[i].status.toLowerCase() == $scope.searchInput.toLowerCase() || 
				   $scope.$parent.menuItemList[i].category.toLowerCase().indexOf($scope.searchInput.toLowerCase()) != -1) {
					$scope.menuItemListHolder.push($scope.$parent.menuItemList[i]);
				}
			}
		}
		else {
			$scope.menuItemListHolder = $scope.$parent.menuItemList;
		}
	}
	
	/**
	 * Used to determine if 'menuItem' can be deleted
	 * Can only delete if an item has never been ordered
	 */
	$scope.canDelete = function(menuItemId) {
		var ok = true;
		for(var i = 0; i < $scope.orderList.length; i++) {
			for(var j = 0; j < $scope.orderList[i].menuItems.length; j++) {
				if($scope.orderList[i].menuItems[j].id == menuItemId) {
					ok = false;
					break;
				}
			}
		}
		return ok;
	}
	
	/**
	 * Used to delete a 'menuItem'
	 */
	$scope.deleteMenuItem = function(menuItemId){
		if($scope.canDelete(menuItemId)==true) {
			var confirmation = window.confirm("Are you sure you want to delete menu item?");
			if (true == confirmation) {
				/* creating an object that will contain the information for deleting a menu item
				 */
				var menu = {
						id: menuItemId
				}
			
				$http.post("/DeleteMenuItem", menu)
					.then(function(response) {
						if (response.data.errorList.length == 0) {
							alert("Successfully Deleted Menu Item");
							$scope.listMenuItems();
						} else {
							var errorMessage = "";
							for (var i = 0; i < response.data.errorList.length; i++) {
								errorMessage += response.data.errorList[i] += "\n";
							}
							alert(errorMessage);
						}
					}, function() {
						alert("An error has occured");
					});
			}
		}
		else
			alert("Can only delete menu item that has never been ordered.")
		
	}

	/**
	 * Used to prepare and open the modal for updating a 'menuItem'
	 */
	$scope.loadMenuItem = function(menuItemId){
		$scope.addUpdate = "UPDATE";
		$scope.updateId = menuItemId;
		$scope.openModal();
	}
	
	$scope.listMenuItems();
	$scope.listOrderItems();
	
	console.log("adminMenu.menuController " + "end");
}]);

app.controller('adminModalController', ['$scope', '$http', '$rootScope', 'ngDialog', function ($scope, $http, $rootScope, ngDialog)  {
	
	$scope.menuItem = {
		id: 0,
		menuItemName: "",
		servings: 0,
		price: 0,
		status: "",
		category: "",
		imagePath: ""
	};
	
	/**
     * Holds the array of menuItem categories that will be used to populate the
     * 'menuItem.category' select element
     */
	$scope.categoryArray = [
	    {optionValue: "mainDish" ,displayValue: "Main Dish"},
	    {optionValue: "drinks" ,displayValue: "Drinks"},
	    {optionValue: "dessert" ,displayValue: "Dessert"}, 
	    {optionValue: "horsDOeuvre" ,displayValue: "Hors d'Oeuvre"}
	];
	
	/**
	 * Setting first option as selected in 'menuItem.category' select
	 */
	$scope.menuItem.category = $scope.categoryArray[0];
	
	/**
	 *  A helper function to get the selected index of category in categoryArray
	 */
	$scope.getIndexCategory = function(category) {
		var index = null;
		for(var i = 0; i < $scope.categoryArray.length; i++) {
			if($scope.categoryArray[i].optionValue == category) {
				index = i;
				break;
			}
		}
		return index;
	}
	
	/**
	 * Used to populate the fields if the user wants to update a menu item
	 */
	$scope.populateFields = function() {
		if($scope.addUpdate == "UPDATE") {
			for(var i = 0; i < $scope.menuItemList.length; i++) {
				if($scope.menuItemList[i].id == $scope.updateId) {
					$scope.menuItem.imagePath = $scope.menuItemList[i].imagePath;
					$scope.menuItem.menuItemName = $scope.menuItemList[i].menuItemName;		
					$scope.menuItem.category = $scope.categoryArray[$scope.getIndexCategory($scope.menuItemList[i].category)];
					$scope.menuItem.servings = $scope.menuItemList[i].servings;
					$scope.menuItem.price = $scope.menuItemList[i].price;
				}
			}
		}
	}
	
	$scope.populateFields();
	
	/**
	 * Used to determine if 'menuItem' can be updated
	 * Cannot update if there is an order associated with the item that is unpaid
	 */
	$scope.canUpdate = function() {
		var ok = true;
		for(var i = 0; i < $scope.orderList.length; i++) {
			for(var j = 0; j < $scope.orderList[i].menuItems.length; j++) {
				if($scope.orderList[i].menuItems[j].id == $scope.updateId &&
				   $scope.orderList[i].status == "unpaid") {
					ok = false;
					break;
				}
			}
		}
		return ok;
	}
	
	/**
	 * Used to add a 'menuItem' to the datastore 
	 */
	$scope.registerMenuItem = function(){
		// ask the user if he/she wants to proceed to inserting the 'menuItem'
		var confirmation = window.confirm("Are you sure you want to insert menu item?");
		if (true == confirmation) {
			var state = "unavailable";
			if($scope.menuItem.servings > 0)
				state = "available";
			/* creating an object that will contain the information for
			   the 'menuItem' input fields.
			*/
			var menu = {
					menuItemName: $scope.menuItem.menuItemName,
					servings: $scope.menuItem.servings,
					price: $scope.menuItem.price,
					status: state,
					category: $scope.menuItem.category.optionValue,
					imagePath: $scope.menuItem.imagePath
			}
				
			// sending the request to the 'AddMenuItemController using AJAX
			$http.post("/AddMenuItem", menu)
				.then(function(response) {
					if (response.data.errorList.length == 0) {
						alert("Inserting menu item was successful.");
						$scope.listMenuItems();
						ngDialog.close();
					}
					else {
						var errorMessage = "";
						for (var i = 0; i < response.data.errorList.length; i++) {
							errorMessage += response.data.errorList[i] += "\n";
						}
						alert(errorMessage);
					}
				}, function() {
					alert("An error has occured.");
				});
		}
	}
	
	/**
	 * Used to update a 'menuItem'
	 */
	$scope.updateMenuItem = function(){
		if($scope.canUpdate()==true) {
				// ask the user if he/she wants to proceed to updating the 'menuItem'
			var confirmation = window.confirm("Are you sure you want to update menu item?");
			if (true == confirmation) {
				var state = "unavailable";
				if($scope.menuItem.servings > 0)
					state = "available";
				/* creating an object that will contain the information for
		   		the 'menuItem' input fields.
				 */
				var menu = {
						id: $scope.updateId,
						menuItemName: $scope.menuItem.menuItemName,
						servings: $scope.menuItem.servings,
						price: $scope.menuItem.price,
						status: state,
						category: $scope.menuItem.category.optionValue,
						imagePath: $scope.menuItem.imagePath
				}
			
				// sending the request to the 'UpdateMenuItemController using AJAX
				$http.post("/UpdateMenuItem", menu)
					.then(function(response) {
					
						if (response.data.errorList.length == 0) {
							alert("Updating menu item was successful.");
							$scope.listMenuItems();
							ngDialog.close();
						}
						else {
							var errorMessage = "";
							for (var i = 0; i < response.data.errorList.length; i++) {
								errorMessage += response.data.errorList[i] += "\n";
							}
							alert(errorMessage);
						}
					}, function() {
						alert("An error has occured.");
					});
			}
		}
		else
			alert("Cannot update a menu item that has an order associated that is unpaid.")
	}
	
	$scope.storeMenuItem = function(){
		if ($scope.addUpdate == "ADD")
			$scope.registerMenuItem();
		else
			$scope.updateMenuItem();
	}
}]);

/**
 * Custom directive to get the path of an image 
 */
app.directive('file', [function () {
	try {
		return {
			require: "ngModel",
			restrict: 'A',
			link: function ($scope, el, attrs, ngModel) {
				el.bind('change', function (event) {
					ngModel.$setViewValue("/assets/Menu Item Images/" + event.target.files[0].name);
					$scope.$apply();
				});

				$scope.$watch(function () {
					return ngModel.$viewValue;
				}, function (value) {
					if (!value) {
						el.val("");
					}
				});
			}
		};
	} catch(Exception ){}
}]);

