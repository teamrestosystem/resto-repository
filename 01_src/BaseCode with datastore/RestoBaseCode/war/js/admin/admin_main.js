/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
 
/*
* This file contains the backend process for the web page.
* The holds the values/properties that is common to the application.
* @author Team Rocket
 * @version 0.01
 * Version History
 * [09/27/2016] 0.01 � Keith Edmond L. Araneta  � Initial codes.
 * [09/11/2016] 0.01 � Keith Edmond L. Araneta  � Added some codes.
 * [10/09/2016] 0.01 Loregge M. Kandt added disable back button
 */


//*** Author: Wael Sidawi
//****  Deactive Back Button **** 
//var history_api = typeof history.pushState !== 'undefined';
//history.pushState must be called out side of AngularJS Code
//if ( history_api ) history.pushState(null, '', '#StayHere');  // After the # you should write something, do not leave it empty

/**
 * The angular module object.
 * @param adminApp - the application name (refer to the 'ng-app' directive)
 */
var app = angular.module('adminApp', ['ngRoute', 'ngDialog']);
/**
* Sets the routing of the screen and controller depending on the url that
* is accessed.
**/
app.config(['$routeProvider', 'ngDialogProvider', function($routeProvider,ngDialogProvider) {
    $routeProvider
      .when('/tableScreen', {
        templateUrl: '/html/templates/admin_screen_table.html',
        controller: 'tableController'
      })
      .when('/orderScreen', {
        templateUrl: '/html/templates/admin_screen_order.html',
        controller: 'orderController'
      })
	  .when('/menuScreen', {
        templateUrl: '/html/templates/admin_screen_menu.html',
        controller: 'menuController'
      })
      .otherwise({
    	 templateUrl: '/html/templates/admin_screen_table.html',
         controller: 'tableController'
      });
    
    ngDialogProvider.setDefaults({
        className: 'ngdialog-theme-default',
        plain: false,
        showClose: true,
        closeByDocument: true,
        closeByEscape: true,
        appendTo: false,
        preCloseCallback: function () {}
      });
}]);
//Gee : di ni mu work. para continuous activate unta ni sa tab but di man mu work
app.controller('tabsController', function($scope, $location) {
    $scope.isActive = function(route) {
        return route === $location.path();
    }
});

/**
 * This controller serves as the holder for the all the other controllers
 * defined in the application. Setting the properties, and methods of the
 * angular controller. The properties and methods inside this controller can be
 * accessed using the '$parent'.
 * 
 * @param adminMainController -
 *            the controller name (refer to the 'ng-controller' directive)
 * @param function() -
 *            the services that will be used in this controller.
 */
app.controller('adminMainController', function($scope) {
	/**
	 * This will hold the list of 'menu item' that are stored statically.
	 */
	$scope.menuItemList = [];
	
	$scope.logout =  function(){
		window.location.href = '/html/customer_screen.html';
	}
});


