/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
 
/*
* This file contains the backend process for the 'Admin Table' screen
* that is under the 'tableController'.
* The holds the values/properties that is common to the application.
* @author Team Rocket
 * @version 0.01
 * Version History
 * [09/07/2016] 0.01 – Keith Edmond L. Araneta  – Initial codes.
 * [09/11/2016] 0.02 � Teresita Tala P. Rabago  � tableList Array.
 * [09/19/2016] 0.02 � Teresita Tala P. Rabago  � search function.
 * [09/21/2016] 0.02 � Teresita Tala P. Rabago  � some codes.
 * [09/29/2016] 0.02 � Teresita Tala P. Rabago  � implemented listTableItems() and addTableItem().
 * [10/04/2016] 0.02 � Teresita Tala P. Rabago  � updated some codes.
 */

app.controller('tableController', ['$scope', '$http', '$rootScope', 'ngDialog', '$filter', function ($scope, $http, $rootScope, ngDialog, $filter) {
	
	var number = (parseInt($scope.tableNo)) + 1;
	$scope.tableItem = {
			id: 0,
			tableNumber: number.toString(),
			tableCapacity: 0,
			tableStatus: "Vacant"
		};
	
	/**
     * Holds the array of tables that will be used to populate 
     * the table in admin_screen_table.html
     */
	$scope.tableList = [];	
	
	$scope.tableNo = 0;
	
	$scope.filteredArrayTable = null;	
	
	$scope.filteringSearchTable = function(){
		if($scope.searchTextTable != null){
			var filtered = [];
		    for (var i = 0; i < $scope.tableList.length; i++) {
		      if($scope.tableList[i].tableNumber.indexOf($scope.searchTextTable) !== -1 || 
		  	     $scope.tableList[i].tableCapacity == $scope.searchTextTable || 
			     $scope.tableList[i].tableStatus.toLowerCase().indexOf($scope.searchTextTable.toLowerCase()) !== -1 && 
			     $scope.tableList[i].tableStatus.toLowerCase().indexOf($scope.searchTextTable.toLowerCase()) === 0){
		        filtered.push($scope.tableList[i]);
		      }
		    }
		    $scope.filteredArrayTable = filtered;
		}
		else
			$scope.filteredArrayTable = $scope.tableList;
	};

	/**
	 * Used to retrieve all table items from the datastore
	 */
	$scope.listTableItems = function() {
		$http.get("/ListTable")
			.then(function(response) {
				if (response.data.errorList.length == 0) {
					$scope.tableList = response.data.tableList;
					$scope.filteredArrayTable = $scope.tableList;
					$scope.tableNo = $scope.tableList.length;
				} else {
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i] += "\n";
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured");
			});
	}
	
	/**
	 * Used to confirm the validity of the table number
	 */
	$scope.checkTableNumber = function() {
		if(number < 10)
			$scope.tableItem.tableNumber = "0" + $scope.tableItem.tableNumber;
	};
	
	/**
	 * Used to add a 'tableItem' to the datastore 
	 */
	$scope.addTableItem = function(){
		// ask the user if he/she wants to proceed to inserting the 'menuItem'
		var confirmation = window.confirm("Are you sure you want to insert a table?");
		if (true == confirmation) {
			/* creating an object that will contain the information for
			   the 'tableItem' input fields.
			*/
			$scope.checkTableNumber();
			
			var table = {
					tableNumber: $scope.tableItem.tableNumber,
					tableCapacity: $scope.tableItem.tableCapacity,
					tableStatus: "Vacant"
			}
			
			// sending the request to the 'AddMenuItemController using AJAX
			$http.post("/AddTable", table)
				.then(function(response) {
					if (response.data.errorList.length == 0) {
						alert("Inserting a table was successful.");
						$scope.listTableItems();
						ngDialog.close();
					}
					else {
						var errorMessage = "";
						for (var i = 0; i < response.data.errorList.length; i++) {
							errorMessage += response.data.errorList[i] += "\n";
						}
						alert(errorMessage);
					}
				}, function() {
					alert("An error has occured.");
				});
		}
	}
	
	/**
	 * Used to open the modal containing the form for adding a 'menuItem'
	 */
	$scope.openModal = function(){
		ngDialog.open({
			template: '/html/templates/admin_screen_table_createTableModal.html',
			scope: $scope,

			controller: 'tableController',

			preCloseCallback:function(){ 
				$scope.addUpdate = "ADD";
				$scope.updateId = null;
			}
		});
	}
	
	$scope.listTableItems();
}]);