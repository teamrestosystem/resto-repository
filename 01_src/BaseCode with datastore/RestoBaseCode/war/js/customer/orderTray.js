/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities and global variables for the application.
* @author Team Rocket
* @version 0.01
* Version History
* [07/26/2016] 0.01  Ryan L. Lobitana  initial codes
* [09/09/2016] 0.02  Kimberly Myles O. Dosdos  implemented functions
* [09/28/2016] 0.02  Ryan L. Lobitana  added the the order object
* [10/09/2016] 0.02  Loregge M. Kandt added disable back button
* [10/14/2016] 0.02  Keith Edmond L. Araneta - code changes
* [10/16/2016] 0.02  Keith Edmond L. Araneta - code changes
* [10/16/2016] 0.02  Loregge M. Kandt - added get time stamp
**/

app.controller('trayController', ['$scope', '$http', '$rootScope', 'ngDialog','$interval', function ($scope, $http, $rootScope, ngDialog,$interval) {
	$scope.startTimer = function(){
		var tick = function() {
			if($scope.cannotCancel == false && $scope.hasChosenATable == true){
				$scope.$parent.timeToCancel -= 1000;
				if($scope.timeToCancel == 0){
					$scope.stopTimer();
					$scope.$parent.cannotCancel = true;
				}
			}
		}
		tick();
		$interval(tick, 1000);
	}
	$scope.stopTimer  = function(){
			$scope.$parent.timeToCancel = 180000;
			$interval.cancel($scope.$parent.timeToCancel);
	}
	
	$scope.startTimer();
	
	/**
	 * Used to open the modal containing the form for submitting an order
	 */
	$scope.openModal = function(){
		if($scope.$parent.hasChosenATable == false) {
			alert("Please choose a table first.")
		}
		else {
			ngDialog.open({
				//template: '/html/templates/customer_screen_PayModal.html',
				template: '/html/templates/customer_screen_TrayModal.html',
				scope: $scope,
				controller: 'trayModalController'
			});
		}
	}
	
	
}]);


app.controller('trayModalController', ['$scope', '$http', '$rootScope', 'ngDialog', function ($scope, $http, $rootScope, ngDialog)  {
	
	$scope.totalCost = 0;
	$scope.status = "Unpaid";
	
	$scope.getTimeStamp = function(){
		var date = new Date();
		var dateString = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
		var timeString = date.getHours() + ':' + date.getMinutes() +':' + date.getSeconds();
		var timeStamp = dateString + ' ' + timeString;
		return timeStamp;
	}
	
	$scope.generateOrderNumber = function() {
		var orderNum = 0;
		orderNum += Math.floor(Math.random() * (9 - 0 + 1)) + 0;
		
		for(var i = 0; i < 10; i++) {
			orderNum  = orderNum * 10;
			orderNum += Math.floor(Math.random() * (9 - 0 + 1)) + 0;
		}
		
		return orderNum;
	}
	
	$scope.getTableNumber = function() {
		var tableNumber = 0;
		for(var i = 0; i < $scope.$parent.$parent.tableList.length; i++) {
			if($scope.$parent.$parent.tableList[i].id == $scope.$parent.$parent.selectedTableId) {
				tableNumber = $scope.$parent.$parent.tableList[i].tableNumber;
				break;
			}
		}
		return tableNumber;
	}
	
	$scope.calculateTotalCost = function() {
		$scope.totalCost = 0;
		for(var i=0; i < $scope.$parent.$parent.orderMenuItemList.length; i++)
			$scope.totalCost += $scope.$parent.$parent.orderMenuItemList[i].price;
	}
	
	$scope.removeOrderItem = function(orderItem) {
		var confirmation = window.confirm("Are you sure you want to remove order item?");
		if (true == confirmation) {
			var index = $scope.$parent.$parent.orderMenuItemList.indexOf(orderItem);
			$scope.$parent.$parent.orderMenuItemList.splice(index, 1);
			$scope.calculateTotalCost();
		}
	}
	
	//todo
	$scope.submitOrder = function() {
		if($scope.$parent.$parent.orderMenuItemList.length == 0) {
			alert("Nothing to submit. Please choose from our menu.")
		}
		else {
			var confirmation = window.confirm("Are you sure you want to submit your order?");
			if (true == confirmation) {
				
				var order = {
						orderNumber: $scope.generateOrderNumber(),
						tableNumber: $scope.getTableNumber(),
						orderTimestamp: $scope.getTimeStamp(),
						menuItems: $scope.$parent.$parent.orderMenuItemList,
						totalCost: $scope.totalCost,
						status: $scope.status
				};
				
				$scope.$parent.$parent.orderHolder = order;
				
				$http.post("/AddOrder", order)
				.then(function(response) {
					if (response.data.errorList.length == 0) {
						alert("Submitting order was successful.");
						$scope.$parent.$parent.isSubmitted = true;
						ngDialog.close();
					}
					else {
						var errorMessage = "";
						for (var i = 0; i < response.data.errorList.length; i++) {
							errorMessage += response.data.errorList[i] += "\n";
						}
						alert(errorMessage);
					}
				}, function() {
					alert("An error has occured.");
				});
			}
		}
	}
	
	$scope.updateOrder = function() {
		
		var order = {
				orderNumber: $scope.$parent.$parent.orderHolder.orderNumber,
				tableNumber: $scope.$parent.$parent.orderHolder.tableNumber,
				orderTimestamp: $scope.$parent.$parent.orderHolder.orderTimeStamp,
				menuItems: $scope.$parent.$parent.orderHolder.menuItems,
				totalCost: $scope.$parent.$parent.orderHolder.totalCost,
				status: "Vacant"
		};
				
				$http.post("/UpdateOrder", order)
				.then(function(response) {
					if (response.data.errorList.length == 0) {
						$scope.$parent.$parent.isSubmitted = true;
						ngDialog.close();
					}
					else {
						var errorMessage = "";
						for (var i = 0; i < response.data.errorList.length; i++) {
							errorMessage += response.data.errorList[i] += "\n";
						}
						alert(errorMessage);
					}
				}, function() {
					alert("An error has occured.");
				});
	}
	
	$scope.payOrder = function() {
		var confirmation = window.confirm("Pay order now?");
		if (true == confirmation) {
			if($scope.inputPrice >= $scope.totalCost) {
				//$scope.updateOrder();
				alert("Successfully paid order.")
				ngDialog.close();
			}
			else {
				alert("Insufficent amount of money.")
			}
		}
	}
	
	$scope.openPayModal = function(){
		ngDialog.open({
			template: '/html/templates/customer_screen_PayModal.html',
			scope: $scope,
			controller: 'trayModalController'
		});
	}
	
	//todo
	$scope.payOrderConfirm = function() {
		if($scope.$parent.$parent.isSubmitted == false) 
			alert("Nothing to pay. Submit your order first.");
		else 
			$scope.openPayModal();
	}
	
	//todo
	$scope.cancelOrder = function() {
		if($scope.$parent.$parent.isSubmitted == false) {
			alert("Nothing to cancel. Submit your order first.");
		}
		else {
			var confirmation = window.confirm("Are you sure you want to cancel your order?");
			if (true == confirmation) {
				alert("Successfully cancelled order.")
			}
		}
	}

	$scope.calculateTotalCost();
}]);
