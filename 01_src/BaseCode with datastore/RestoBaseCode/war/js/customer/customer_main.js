/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/

/**
 * This file contains the functionalities and global variables for the
 * application.
 * 
 * @author Team Rocket
 * @version 0.01 Version History 
 * [07/26/2016] 0.01 Ryan L. Lobitana initial codes 
 * [10/09/2016] 0.02 Loregge M. Kandt added disable back button [not yet working tho]
 * [10/14/2016] 0.02  Keith Edmond L. Araneta - code changes
 * [10/16/2016] 0.02  Keith Edmond L. Araneta - code changes
 */

/**
 * The angular module object.
 * @param customerApp - the application name (refer to the 'ng-app' directive)
 */
var app = angular.module('customerApp', [ 'ngRoute', 'ui.bootstrap', 'ngDialog' ]);

// *** Author: Wael Sidawi
// **** Deactive Back Button ****
//var history_api = typeof history.pushState !== 'undefined';
// history.pushState must be called out side of AngularJS Code
//if (history_api)
//	history.pushState(null, '', '#StayHere'); // After the # you should write
												// something, do not leave it
												// empty

/**
 * Sets the routing of the screen and controller depending on the url that is
 * accessed.
 */
app.config([ '$routeProvider', 'ngDialogProvider',
		function($routeProvider, ngDialogProvider) {
			$routeProvider.when('/dishScreen', {
				templateUrl : '/html/templates/customer_screen_dish.html',
				controller : 'dishController'
			}).when('/drinkScreen', {
				templateUrl : '/html/templates/customer_screen_drink.html',
				controller : 'drinkController'
			}).when('/dessertScreen', {
				templateUrl : '/html/templates/customer_screen_dessert.html',
				controller : 'dessertController'
			}).when('/appetizerScreen', {
				templateUrl : '/html/templates/customer_screen_appetizer.html',
				controller : 'appetizerController'
			}).otherwise({
				templateUrl : '/html/templates/customer_screen_home.html',
				controller : 'homeController'
			});

			ngDialogProvider.setDefaults({
				className : 'ngdialog-theme-default',
				plain : false,
				showClose : true,
				closeByDocument : true,
				closeByEscape : true,
				appendTo : false,
				preCloseCallback : function() {}
			});

		} ]);

/**
 * This controller serves as the holder for the all the other controllers
 * defined in the application. Setting the properties, and methods of the
 * angular controller. The properties and methods inside this controller can be
 * accessed using the '$parent'.
 * 
 * @param customerMainController -
 *            the controller name (refer to the 'ng-controller' directive)
 * @param function() -
 *            the services that will be used in this controller.
 */
app.controller('customerMainController', function($scope,$http) {
	/**
	 * True if the user already confirmed his/her chosen table
	 */
	$scope.hasChosenATable = false;
	/**
	 * Holds the id of the selected table
	 */
	$scope.selectedTableId = null;
	/**
     * Holds the array of tables that will be used to populate 
     * the table in customer_screen_home.html
     */
	$scope.tableList = [];
	$scope.tableListHolder = [];
	
	$scope.orderHolder = null;
	/**
	 * The holder of the list of all menu items ordered by the customer
	 * */
	$scope.orderMenuItemList = [];
	
	$scope.isSubmitted = false;
	
	$scope.getTable = function(status) {
		var index = 0;
		for(var i = 0; i < $scope.tableList.length; i++) {
			if($scope.tableList[i].id == $scope.selectedTableId) {
				index = i;
				break;
			}
		}
		var table = {
			id: $scope.tableList[index].id,
			tableNumber: $scope.tableList[index].tableNumber,
			tableCapacity: $scope.tableList[index].tableCapacity,
			tableStatus: status
		};
		return table;
	}
	
	/**
	 * Used to update a 'table'
	 */
	$scope.updateTable = function(status, tableId){
		/* creating an object that will contain the information for
		the 'table' input fields.
		*/
		var table = $scope.getTable(status);
			
		// sending the request to the 'UpdateTableController using AJAX
		$http.post("/UpdateTable", table)
			.then(function(response) {
					
				if (response.data.errorList.length == 0) {}
				else {
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i] += "\n";
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured.");
			});
	}
	
	$scope.openLogin = function() {
		if($scope.hasChosenATable == true) {
			var confirmation = window.confirm("Proceed with log in?");
			if (true == confirmation) {
				$scope.updateTable("Vacant");
				$scope.orderMenuItemList = [];
				$scope.hasChosenATable = false;
				$scope.selectedTableId = null;
				$scope.tableList = [];
				$scope.tableListHolder = [];
			}
		}
		window.location.href = '/html/login_screen.html';
	}
	/**
	 * holds the available time to cancel an order which is 3 minutes (180000 milliseconds)
	 */
	$scope.timeToCancel = 180000;
	/**
	 * True if the user already occupied a table for 3 minutes , therefore, he/she cannot cancel his/her order
	 */
	$scope.cannotCancel = false;
});

app.controller('timeCtrl', function($scope, $interval) {
	var tick = function() {
		$scope.clock = Date.now();
	}
	tick();
	$interval(tick, 1000);
});

//wont work gonna solve this after all the validations done 
app.controller('tabsCtrl', ['$scope', function ($scope) {
    $scope.tabs = [{
            title: 'Home',
            url: '/html/templates/customer_screen_home.html'
        }, {
            title: 'Main Dish',
            //url: 'two.tpl.html'
        }, {
            title: 'Drinks',
            //url: 'three.tpl.html'
        }, {
        	title: "Dessert"
        }, {
        	title:"Hors d' Ourve"
        }];

    $scope.currentTab = '/html/templates/customer_screen_home.html';

    $scope.onClickTab = function (tab) {
        $scope.currentTab = tab.url;
    }
    
    $scope.isActiveTab = function(tabUrl) {
        return tabUrl == $scope.currentTab;
    }
}]);