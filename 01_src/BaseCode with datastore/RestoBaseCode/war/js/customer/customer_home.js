/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Rococo Global Technologies, Inc - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
 
/*
* This file contains the backend process for the 'Customer Home' screen
* that is under the 'homeController'.
 * @version 0.01
 * Version History
 * [04/13/2016] 0.01 – Loregge Kandt  – Initial codes.
 * [10/14/2016] 0.01 – Keith Edmond L. Araneta  – code changes.
 * [10/16/2016] 0.01 – Keith Edmond L. Araneta  – code changes.
 * [10/16/2016] 0.01 – Loregge M. Kandt  – added function that cannot let cancel in 3 minutes.
 */


app.controller('homeController', ['$scope', '$http', '$rootScope', 'ngDialog','$interval', function ($scope, $http, $rootScope, ngDialog,$interval) {
	$scope.flag =false;
	$scope.filteringSearchTable = function(){
		if($scope.searchTextTable != null){
			var filtered = [];
		    for (var i = 0; i < $scope.$parent.tableListHolder.length; i++) {
		    	if($scope.$parent.tableListHolder[i].tableCapacity == $scope.searchTextTable){
		    		filtered.push($scope.$parent.tableListHolder[i]);
		    	}
		    }
		    $scope.filteredArrayTable = filtered;
		}
		else{
			$scope.filteredArrayTable = $scope.$parent.tableListHolder;
		}
	};
	
	
	/**
	 * Used to retrieve all table items from the datastore
	 */
	$scope.listTables = function() {
		if($scope.$parent.hasChosenATable == false){
			$http.get("/ListTable")
				.then(function(response) {
					if (response.data.errorList.length == 0) {
						$scope.$parent.tableList = [];
						$scope.$parent.tableListHolder = [];
						$scope.$parent.selectedTableId = null;
						$scope.$parent.tableList = response.data.tableList;
						for(var i = 0; i < response.data.tableList.length; i++){
							if(response.data.tableList[i].tableStatus == "Vacant")
							{
								$scope.$parent.tableListHolder.push(response.data.tableList[i]);
							}
						}
						$scope.filteredArrayTable = $scope.$parent.tableListHolder;
						//$scope.tableNo = $scope.tableList.length;
					} else {
						var errorMessage = "";
						for (var i = 0; i < response.data.errorList.length; i++) {
							errorMessage += response.data.errorList[i] += "\n";
						}
						alert(errorMessage);
					}
				}, function() {
					alert("An error has occured");
				});
		}
		else if($scope.$parent.hasChosenATable == true){
			$scope.filteredArrayTable = [];
			for(var i = 0; i < $scope.$parent.tableListHolder.length; i++) {
				if($scope.$parent.tableListHolder[i].id == $scope.$parent.selectedTableId) {
					$scope.filteredArrayTable.push($scope.$parent.tableListHolder[i]);
					break;
				}
			}
		}
	}
	
	
	$scope.getTable = function(status, tableId) {
		var index = 0;
		if(status == "Occupied") {
			for(var i = 0; i < $scope.$parent.tableList.length; i++) {
				if($scope.$parent.tableList[i].id == $scope.$parent.selectedTableId) {
					index = i;
					break;
				}
			}
		}
		else {
			for(var i = 0; i < $scope.$parent.tableList.length; i++) {
				if($scope.$parent.tableList[i].id == tableId) {
					index = i;
					break;
				}
			}
		}
		
		var table = {
			id: $scope.$parent.tableList[index].id,
			tableNumber: $scope.$parent.tableList[index].tableNumber,
			tableCapacity: $scope.$parent.tableList[index].tableCapacity,
			tableStatus: status
		};
		return table;
	}
	
	/**
	 * Used to update a 'table'
	 */
	$scope.updateTable = function(status, tableId){
		/* creating an object that will contain the information for
		the 'table' input fields.
		*/
		var table = $scope.getTable(status, tableId);
		
		// sending the request to the 'UpdateTableController using AJAX
		$http.post("/UpdateTable", table)
			.then(function(response) {
					
				if (response.data.errorList.length == 0) {
					$scope.listTables();
				}
				else {
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i] += "\n";
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured.");
			});
	}
	
	/**
	 * Used to open the modal containing the form for selecting a 'table'
	 */
	$scope.openModal = function(){
		ngDialog.open({
			template: '/html/templates/customer_screen_home_selectTable.html',
			scope: $scope,
			controller: 'customerModalController',
		});
	}
	
	/**
	 * Used to prepare and open the modal for selecting a 'table'
	 */
	$scope.selectTable = function(tableId){
		$scope.$parent.selectedTableId = tableId;
		$scope.openModal();
	}
	
	$scope.cancelTable = function(tableId){
		var confirmation = window.confirm("Are you sure you want to cancel?");
		if (true == confirmation) {
			$scope.$parent.timeToCancel = 180000;
			$scope.$parent.hasChosenATable = false;
			$scope.$parent.orderMenuItemList = [];
			$scope.updateTable("Vacant", tableId);
			//$scope.listTables();
		}
	}
	
	$scope.listTables();
	
	$scope.gallery = [
	  {src: '/assets/Menu Item Images/Beef Steak with Asparagus and Peanuts.jpg', title: 'Beef Steak with Asparagus and Peanuts'}, 
	  {src: '/assets/Menu Item Images/Spaghetti and Meatballs.jpg', title: 'Spaghetti and Meatballs'}, 
	  {src: '/assets/Menu Item Images/Ice Cream Deluxe.jpg', title: 'Ice Cream Deluxe'},
	  {src: '/assets/Menu Item Images/4.jpg', title: 'Pic 4'}, 
	  {src: '/assets/Menu Item Images/Fried Chicken.jpg', title: 'Fried Chicken'},
	  {src: '/assets/Menu Item Images/Burger and Fries.jpg', title: 'Burger and Fries'}
	  ];
	}]).directive('gallery', function(){
                return {
                  restrict: 'E',
                  template: '<img ng-repeat="image in gallery" ng-src="{{image.src}}" class="gallery-item" ng-show="isActive($index)">'+
                    '<a class="arrow prev" href="#" ng-click="showPrev()"><img class="arrowImg" src="../assets/previous.png"/></a>'
          +
                    '<a class="arrow next" href="#" ng-click="showNext()"><img class="arrowImg" src="../assets/next.png"/></a>',
                  controller: ['$scope', '$http', function($scope, $http) {
                    $scope._Index = 0;
             
                     // if a current image is the same as requested image
                      $scope.isActive = function (index) {
                          return $scope._Index === index;
                      };
                   
                      // show prev image
                      $scope.showPrev = function () {
                          $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.gallery.length - 1;
                      };
                   
                      // show next image
                      $scope.showNext = function () {
                          $scope._Index = ($scope._Index < $scope.gallery.length - 1) ? ++$scope._Index : 0;
                      };
                  }]
                };
});

app.controller('customerModalController', ['$scope', '$http', '$rootScope', 'ngDialog', function ($scope, $http, $rootScope, ngDialog)  {
	$scope.table = {
			id: 0,
			tableNumber: "",
			tableCapacity: 0,
			tableStatus: ""
	};
	
	/**
	 * Used to get the data of the selected table
	 */
	$scope.populateTableData = function() {
		for(var i = 0; i < $scope.$parent.tableListHolder.length; i++) {
			if($scope.$parent.tableListHolder[i].id == $scope.$parent.selectedTableId) {
				$scope.table.tableNumber = $scope.$parent.tableListHolder[i].tableNumber;
				$scope.table.tableCapacity = $scope.$parent.tableListHolder[i].tableCapacity;
				break;
			}
		}
	}
	$scope.populateTableData();
	
	$scope.closeModal = function(){
		ngDialog.close();
	}
	
	/**
	 * Used if the user confirmed a table
	 */
	$scope.confirmTable = function(){
		$scope.$parent.$parent.hasChosenATable = true;
		$scope.updateTable("Occupied", 0);
		ngDialog.close();
	}
}]);
/*
app.controller('timeToCancelCtrl', ['$scope', '$http', '$rootScope', 'ngDialog','$interval', function ($scope, $http, $rootScope, ngDialog,$interval) {
	$scope.table = {
			id: 0,
			tableNumber: "",
			tableCapacity: 0,
			tableStatus: ""
		};
	$scope.timerStopped = false;
	$scope.startTimer = function(){
		console.log("starttmer st");
		var tick = function() {
			if($scope.cannotCancel == false && $scope.hasChosenATable == true){
				$scope.$parent.$parent.timeToCancel -= 1000;
				if($scope.timeToCancel == 0){
					$scope.stopTimer();
					$scope.$parent.$parent.cannotCancel = true;
				}
			}
		}
		tick();
		$interval(tick, 1000);
		console.log("starttmer end");
	}
	$scope.stopTimer  = function(){
			$scope.$parent.$parent.timeToCancel = 180000;
			$scope.timerStopped = true;
			$interval.cancel($scope.$parent.$parent.timeToCancel);
	}
	
	$scope.startTimer();
	
	
	$scope.cancelTable = function(){
		console.log("customer_home.homeController.cancelTable " + "start");
		if($scope.$parent.$parent.orderMenuItemList.length == 0) {
			$scope.confirmCancel();
		}
		else
		{
			if($scope.timerStopped == false)
				$scope.confirmCancel();
		}
		
		console.log("customer_home.homeController.cancelTable " + "end");
	}
	
	
	
	$scope.confirmCancel = function(){
		var confirmation = window.confirm("Are you sure you want to cancel?");
		if (true == confirmation) {
			
			/**
			 * Used to get the data of the selected table
			 
			$scope.populateTableData = function() {
				for(var i = 0; i < $scope.$parent.tableList.length; i++) {
					if($scope.$parent.tableList[i].id == $scope.$parent.selectedTableId) {
						$scope.table.id = $scope.$parent.tableList[i].id;
						$scope.table.tableNumber = $scope.$parent.tableList[i].tableNumber;
						$scope.table.tableCapacity = $scope.$parent.tableList[i].tableCapacity;
						break;
					}
				}
			}
			$scope.populateTableData();
			
			$scope.stopTimer();
			$scope.$parent.$parent.$parent.hasChosenATable = false;
			$scope.$parent.$parent.cannotCancel = false;
			
			
			var tableItem = {
					id: $scope.table.id,
					tableNumber: $scope.table.tableNumber,
					tableCapacity: $scope.table.tableCapacity,
					tableStatus: "Vacant"
			}
			
			// sending the request to the 'UpdateTableController using AJAX
			$http.post("/UpdateTable", tableItem)
				.then(function(response) {
					
					if (response.data.errorList.length == 0) {
						$scope.$parent.listTables();
						$scope.$parent.$parent.orderMenuItemList = [];
					}
					else {
						var errorMessage = "";
						for (var i = 0; i < response.data.errorList.length; i++) {
							errorMessage += response.data.errorList[i] += "\n";
						}
						alert(errorMessage);
					}
				}, function() {
					alert("An error has occured.");
				});
		}
	}
	
}]);*/
