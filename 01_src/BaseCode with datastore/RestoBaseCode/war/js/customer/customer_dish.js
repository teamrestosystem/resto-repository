/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities and global variables for the application.
* @author Team Rocket
* @version 0.01
* Version History
* [07/26/2016] 0.01 – Ryan L. Lobitana – initial codes
* [10/09/2016] 0.02 – Loregge M. Kandt – added modal 
* [10/14/2016] 0.02  Keith Edmond L. Araneta - code changes
* [10/14/2016] 0.03 - Loregge M. Kandt - added search
**/
app.controller('dishController', ['$scope', '$http', '$rootScope', 'ngDialog', function ($scope, $http, $rootScope, ngDialog)  {
		
		$scope.dishId = 0;
		$scope.dishList = [];
		
		$scope.openModal = function(appId){
			if($scope.$parent.hasChosenATable == false) {
				alert("Please choose a table first.")
			}
			else {
				$scope.dishId = appId;
				ngDialog.open({
					template: '/html/templates/customer_screen_MenuItemModal.html',
					scope: $scope,
					controller: 'dishModalController',
				});
			}
		};
		
		/**
		 * Used to retrieve menu items where its category is "mainDish" from the datastore
		 */
		$scope.listMenuItems = function() {
			var j = 0;
			$http.get("/ListMenuItem")
				.then(function(response) {
					if (response.data.errorList.length == 0) {
						for (var i = 0; i < response.data.menuItemList.length; i++) {
							if(response.data.menuItemList[i].category == "mainDish"){
								$scope.dishList.push(response.data.menuItemList[i]);
								
							}
								
						}
						$scope.dishItemListHolder = $scope.dishList;
					} else {
						var errorMessage = "";
						for (var i = 0; i < response.data.errorList.length; i++) {
							errorMessage += response.data.errorList[i] += "\n";
						}
						alert(errorMessage);
					}
				}, function() {
					alert("An error has occured");
				});
		}
		/**
		 * Used to filter the content of the table holding the menu items during search
		 */
		$scope.filteringSearchMenuItem = function() {
			if($scope.searchMenuItem != null) {
				$scope.dishItemListHolder = [];
				for(var i = 0; i < $scope.dishList.length; i++) {
					if($scope.dishList[i].menuItemName.toLowerCase().indexOf($scope.searchMenuItem.toLowerCase()) != -1 ||
					   $scope.dishList[i].status.toLowerCase() == $scope.searchMenuItem.toLowerCase()){
						$scope.dishItemListHolder.push($scope.dishList[i]);
					}
				}
			}
			else {
				$scope.dishItemListHolder = $scope.dishList;
			}
		}
		$scope.listMenuItems();
}]);

app.controller('dishModalController', ['$scope', '$http', '$rootScope', 'ngDialog', function ($scope, $http, $rootScope, ngDialog)  {
	$scope.menuItem = {
			id: 0,
			menuItemName: "",
			servings: 0,
			price: 0,
			status: "",
			category: "",
			imagePath: ""
		};
	/**
	 * Used to populate the fields for menu item Modal
	 */
	$scope.populateFields = function() {
			for(var i = 0; i < $scope.dishList.length; i++) {
				if($scope.dishList[i].id == $scope.dishId) {
					$scope.menuItem.id = $scope.dishList[i].id;
					$scope.menuItem.menuItemName = $scope.dishList[i].menuItemName;
					$scope.menuItem.servings = $scope.dishList[i].servings;
					$scope.menuItem.price = $scope.dishList[i].price;
					$scope.menuItem.status = $scope.dishList[i].status;
					$scope.menuItem.category = $scope.dishList[i].category;
					$scope.menuItem.imagePath = $scope.dishList[i].imagePath;
					break;
				}
			}
	}
	
	$scope.populateFields();
	
	$scope.addDishToTray = function(menuItem) {
		$scope.$parent.$parent.orderMenuItemList.push(menuItem);
		alert("Successfully added item to tray.")
		ngDialog.close();
	}

}]);