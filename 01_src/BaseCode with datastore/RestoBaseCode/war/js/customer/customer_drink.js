/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities and global variables for the application.
* @author Team Rocket
* @version 0.01
* Version History
* [07/26/2016] 0.01 – Ryan L. Lobitana – initial codes
* [10/09/2016] 0.02 – Loregge M. Kandt – added modal 
* [10/14/2016] 0.02  Keith Edmond L. Araneta - code changes
* [10/14/2016] 0.03 - Loregge M. Kandt - added search
**/
app.controller('drinkController', ['$scope', '$http', '$rootScope', 'ngDialog', function ($scope, $http, $rootScope, ngDialog)  {
		
		$scope.drinkId = 0;
		$scope.drinkList = [];
		
		$scope.openModal = function(appId){
			
			if($scope.$parent.hasChosenATable == false) {
				alert("Please choose a table first.")
			}
			else {
				$scope.drinkId = appId;
				ngDialog.open({
					template: '/html/templates/customer_screen_MenuItemModal.html',
					scope: $scope,
					controller: 'drinkModalController',
				});
			}
		
		};
			
		/**
		 * Used to retrieve menu items where its category is "drinks" from the datastore
		 */
		$scope.listMenuItems = function() {
			var j = 0;
			$http.get("/ListMenuItem")
				.then(function(response) {
					if (response.data.errorList.length == 0) {
						for (var i = 0; i < response.data.menuItemList.length; i++) {
							if(response.data.menuItemList[i].category == "drinks"){
								$scope.drinkList.push(response.data.menuItemList[i]);
							}						
						}
						$scope.drinkItemListHolder = $scope.drinkList;
					} else {
						var errorMessage = "";
						for (var i = 0; i < response.data.errorList.length; i++) {
							errorMessage += response.data.errorList[i] += "\n";
						}
						alert(errorMessage);
					}
				}, function() {
					alert("An error has occured");
				});
		}
		/**
		 * Used to filter the content of the table holding the menu items during search
		 */
		$scope.filteringSearchMenuItem = function() {
			if($scope.searchMenuItem != null) {
				$scope.drinkItemListHolder = [];
				for(var i = 0; i < $scope.drinkList.length; i++) {
					if($scope.drinkList[i].menuItemName.toLowerCase().indexOf($scope.searchMenuItem.toLowerCase()) != -1 ||
					   $scope.drinkList[i].status.toLowerCase() == $scope.searchMenuItem.toLowerCase()){
						$scope.drinkItemListHolder.push($scope.drinkList[i]);
					}
				}
			}
			else {
				$scope.drinkItemListHolder = $scope.drinkList;
			}
		}	
		$scope.listMenuItems();
}]);


app.controller('drinkModalController', ['$scope', '$http', '$rootScope', 'ngDialog', function ($scope, $http, $rootScope, ngDialog)  {
	$scope.menuItem = {
			id: 0,
			menuItemName: "",
			servings: 0,
			price: 0,
			status: "",
			category: "",
			imagePath: ""
		};
	/**
	 * Used to populate the fields for menu item Modal
	 */
	$scope.populateFields = function() {
		for(var i = 0; i < $scope.drinkList.length; i++) {
			if($scope.drinkList[i].id == $scope.drinkId) {
				$scope.menuItem.id = $scope.drinkList[i].id;
				$scope.menuItem.menuItemName = $scope.drinkList[i].menuItemName;
				$scope.menuItem.servings = $scope.drinkList[i].servings;
				$scope.menuItem.price = $scope.drinkList[i].price;
				$scope.menuItem.status = $scope.drinkList[i].status;
				$scope.menuItem.category = $scope.drinkList[i].category;
				$scope.menuItem.imagePath = $scope.drinkList[i].imagePath;
				break;
			}
		}
	}
	
	$scope.populateFields();
	
	$scope.addDishToTray = function(menuItem) {
		$scope.$parent.$parent.orderMenuItemList.push(menuItem);
		alert("Successfully added item to tray.")
		ngDialog.close();
	}

}]);
