/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
 
/*
* This file contains the backend process for the 'Log in' screen
* that is under the 'loginController'.
* @author Team Rocket
 * @version 0.01
 * Version History
 * [09/07/2016] 0.01 – Keith Edmond L. Araneta  – Initial codes.
 * [10/09/2016] 0.02 – Loregge M. Kandt  – added home button.
 */


var app = angular.module('loginApp', ['ngRoute']);

app.controller('loginController', function($scope) {
	
	$scope.login = function() {
		var username = $scope.username;
		var password = $scope.password;
		
		if(username == "admin" && password == "okradol")
			window.location.href = '/html/admin_screen.html';
		else 
		   alert("Invalid Username or Password!")
	}
	
	$scope.openHome = function() {
		window.location.href = '/html/customer_screen.html';
	}
});
