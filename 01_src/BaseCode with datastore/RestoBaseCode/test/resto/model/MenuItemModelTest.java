package resto.model;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class MenuItemModelTest extends AppEngineTestCase {

    private MenuItemModel model = new MenuItemModel();

    @Test
    public void test() throws Exception {
        assertThat(model, is(notNullValue()));
    }
}
