/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities and global variables for the application.
* @author Team Rocket
* @version 0.01
* Version History
* [07/26/2016] 0.01 – Ryan L. Lobitana – initial codes
**/

/**
* This function will set the header of the content-screen div
**/
function btnDish(){
	//var header = document.getElementById('content-header');
	//header.innerHTML = "Main Dish";
	//var parentNode = document.getElementsByClassName("content-screen");
	//while (parentNode.firstChild) {
	//	parentNode.removeChild(parentNode.firstChild);
	//}
	var header = document.createElement('h1');
	header.id = "content-header";
	header.innerHTML = "Main Dish";
	parentNode[0].appendChild(header);
	var hRule = document.createElement('hr');
	parentNode[0].appendChild(hRule);
	populateDishImage();
	
}
/**
* Populating the div and its respective images
* Setting of the necessary attributes accompanied in each element
**/
function populateDishImage() {
	console.log("Populating the images");
	
	//var contentScreen = document.getElementsByTagName('content-screen');
	//create a div with an image inside every loop of the dishArray
	for(var i=0; i<dishArray.length; i++){
		var imageLocation = "..\\assets\\";
		//alert("blabla" + i);
		//creating the div element
		var iDiv = document.createElement('div');
		iDiv.className = "dish-content dish-left";
		iDiv.id = 'myBtn';
		iDiv.value = "val1";
		//iDiv.setAttribute("class", "labelMenu");
		//appending the div to content-screen
		document.getElementsByClassName('content-screen')[0].appendChild(iDiv);
		//document.getElements
		
		//creating the label wla pay ccs
		//var dishLabel = document.createElement('label');
		//dishLabel.innerHTML = "" + dishArray[i].dishName;
		//dishLabel.setAttribute("class", "labelMenu");
		//iDiv.appendChild(dishLabel);
		
		//creating the image element
		var dishImage = document.createElement('IMG');
		//dishImage.className = "dishType"; 
		dishImage.id = i;
		//dishImage.value = "val" + i;
		//add the event on every image
		dishImage.onclick = function() {
								openModal('dishModal', this);
							};
		//setting the image path accordingly
		
		switch (i) {
			case 0:
				imageLocation += "drink1.jpg";				
				//dishImage.setAttribute("src","..\\assets\\drink1.jpg");				
				break;
			case 1:
				imageLocation += "shake.jpg";
				//dishImage.setAttribute("src","..\\assets\\shake.jpg");
				break;
			case 2:
				imageLocation += "drink1.jpg";
				//dishImage.setAttribute("src","..\\assets\\drink1.jpg");
				break;
			case 3:
				imageLocation += "shake.jpg";
				//dishImage.setAttribute("src","..\\assets\\shake.jpg");
				break;
			case 4:
				imageLocation += "drink1.jpg";
				//dishImage.setAttribute("src","..\\assets\\drink1.jpg");
				break;
			case 5:
				imageLocation += "shake.jpg";				
				//dishImage.setAttribute("src","..\\assets\\shake.jpg");
				break;
		}
		dishArray[i].imgLocation = imageLocation;
		dishImage.src = imageLocation;
		//appending the dishImage to the iDiv		
		iDiv.appendChild(dishImage);
		
		//imgDishTypeField.src = imageLocation;
	}
	console.log("End of Array");
}