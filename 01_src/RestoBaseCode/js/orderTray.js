/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities and global variables for the application.
* @author Team Rocket
* @version 0.01
* Version History
* [07/26/2016] 0.01 – Ryan L. Lobitana – initial codes
**/

/**
* Global Variable declaration
**/

var orderTray = [];

/**
* This function will set the header of the content-screen div
**/

function addToTray(product){
	confirm("Are you sure?");
	//alert('ff');
	//console.log("Bla");
	alert(product.id);
	if(dishArray[product.id].type == "dishType"){
		var prodName = dishArray[product.id].dishName;
		var prodPrice = dishArray[product.id].dishPrice;
		var prodType = "dishType";
		console.log(prodName + " " + prodPrice);
	}
	else if(drinkArray[product.id].type == "drinkType"){
		var prodName = drinkArray[product.id].drinkName;
		var prodPrice = drinkArray[product.id].drinkPrice;
		var prodType = "dishType";
		console.log(prodName + " " + prodPrice);
	}
	else if(dessertArray[product.id].type == "dessertType"){
		var prodName = dessertArray[product.id].dessertName;
		var prodPrice = dessertArray[product.id].dessertPrice;
		var prodType = "dessertType";
		console.log(prodName + " " + prodPrice);
	}
	else if(appetizerArray[product.id].type == "appetizerType"){
		var prodName = appetizerArray[product.id].appetizerName;
		var prodPrice = appetizerArray[product.id].appetizerPrice;
		var prodType = "appetizerType";
		console.log(prodName + " " + prodPrice);
	}
	console.log(prodName + " " + prodPrice);
	//var nameField = document.getElementById("productName");
	//console.log(nameField.value);
	//var typeField = document.getElementById("productType");
	//var priceField = document.getElementById("productPrice");
	//console.log(priceField.value);
	// storing the values of the each corresponding field into an object
	var orderItem = { 
		//id: dishArray.length, 
		orderName: prodName,
		orderType: prodType, 
		orderPrice: prodPrice
	};
	orderTray.push(orderItem);
	//confirm(orde.productName + "");
	//orderTray.push(dishItem);
	//confirm("orderTray" + dishItem.productName +"\n" + dishItem.productPrice);
	
	updateCartTable();
}

function updateCartTable(){
	var count = orderTray.length;
	alert("count"+count);
	var tableOrder = document.getElementById('tblOrder');
	var row = tableOrder.rows.length;
	alert("Row"+row);
	var tblRow = tableOrder.insertRow(row);
	var tblCellOrderName = tblRow.insertCell(0);
	tblCellOrderName.innerHTML = orderTray[count-1].orderName;
	alert(orderTray[count-1].orderName);
	var tblCellOrderType = tblRow.insertCell(1);
	tblCellOrderType.innerHTML = orderTray[count-1].orderType;
	var tblCellOrderPrice = tblRow.insertCell(2);
	tblCellOrderPrice.innerHTML = orderTray[count-1].orderPrice;
	var tblCellAction = tblRow.insertCell(3);
	var btn = document.createElement('button');    
	var txt = document.createTextNode("REMOVE");	
	btn.appendChild(txt);
	tblCellAction.appendChild(btn);
	
	btn.onclick = function(){ 
		var i=this.parentNode.parentNode.rowIndex;
		document.getElementById('tblOrder').deleteRow(i);
	};

}

