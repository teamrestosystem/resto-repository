/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities for the 'Admin Table' screen.
* @author Team Rocket
* @version 0.01
* Version History
* [07/25/2016] 0.01 – Keith Edmond L. Araneta – initial codes
* [07/27/2016] 0.01 – Teresita Tala P. Rabago – edited some codes
* [07/27/2016] 0.02 – Keith Edmond L. Araneta – added functionalities
**/

/**
* This function is used to initialize the elements in the 'Admin Table' screen.
* Clears the input field.
* Displays necessary elements needed for the screen.
* Hides unnecessay elements from the screen.
**/
function initAdminTableScreen() {
	console.log("restoTable.initAdminTableScreen" + " start");
	
	// hides unnecessary elements 
	document.getElementById("createMenuBtn").style.visibility = "hidden";
	document.getElementById("createMenuBtn").style.display = "inline";
	
	document.getElementById("searchField").style.display = "inline";
	document.getElementById("searchField").value = null;
	document.getElementById("searchField").placeholder = "Table No. / Capacity / Status";
	document.getElementById("searchBtn").style.display = "inline";
	
	console.log("restoTable.initAdminTableScreen" + " end");
}