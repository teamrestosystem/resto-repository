/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities and global variables for the application.
* @author Team Rocket
* @version 0.01
* Version History
* [07/11/2016] 0.01 – Teresita Tala P. Rabago – initial codes
* [07/20/2016] 0.02 – Loregge M. Kandt – added Login function
* [07/25/2016] 0.03 - Keith Edmond L. Araneta - added/organized some codes
* [07/27/2016] 0.03 – Teresita Tala P. Rabago – added some codes
* [07/27/2016] 0.03 – Keith Edmond L. Araneta – added some codes**/

/**
* These are the global variables
**/

/**
* The array that will hold the list of 'dishes' that 
* are statically added in the application.
* The items for the array are mocked.
**/
var dishArray = [
	{id:0, dishName: "Grilled Beef", dishDesc: "mainDish", type: "dishType", dishPrice:100, imgLocation:""},
	{id:1, dishName: "Onigiri", dishDesc: "sideDish", type: "dishType",dishPrice: 80, imgLocation:""},
	{id:2, dishName: "Miso Soup", dishDesc: "soup", type: "dishType",dishPrice: 50, imgLocation:""},
	{id:3, dishName: "Vegetable Soup", dishDesc: "soup", type: "dishType",dishPrice: 40, imgLocation:""},
	{id:4, dishName: "Kinpira Gobo", dishDesc: "sideDish", type: "dishType",dishPrice: 70, imgLocation:""},
	{id:5, dishName: "Chicken Katsu", dishDesc: "mainDish", type: "dishType",dishPrice: 110, imgLocation:""},	
];
/**
* The array that will hold the list of 'drinks' that 
* are statically added in the application.
* The items for the array are mocked.
**/
var drinkArray = [
	{id:0, drinkName: "Beef Teriyaki", drinkDesc: "mainDish", type: "drinkType",drinkPrice:100, imgLocation:""},
	{id:1, drinkName: "Onigiri", drinkDesc: "sideDish",type: "drinkType", drinkPrice: 80, imgLocation:""},
	{id:2, drinkName: "Miso Soup", drinkDesc: "soup",type: "drinkType", drinkPrice: 50, imgLocation:""},
	{id:3, drinkName: "Vegetable Soup", drinkDesc: "soup",type: "drinkType", drinkPrice: 40, imgLocation:""},
	{id:4, drinkName: "Kinpira Gobo", drinkDesc: "sideDish", type: "drinkType",drinkPrice: 70, imgLocation:""},
	{id:5, drinkName: "Chicken Katsu", drinkDesc: "mainDish", type: "drinkType",drinkPrice: 110, imgLocation:""},
];
/**
* The array that will hold the list of 'appetizer' that 
* are statically added in the application.
* The items for the array are mocked.
**/
var appetizerArray = [
	{id:0, appetizerName: "Beef Teriyaki", 	appetizerDesc: "mainDish", 	type: "appetizerType",	appetizerPrice:100, 	imgLocation:""},
	{id:1, appetizerName: "Onigiri",       	appetizerDesc: "sideDish",	type: "appetizerType", 	appetizerPrice: 80, 	imgLocation:""},
	{id:2, appetizerName: "Miso Soup", 		appetizerDesc: "soup",		type: "appetizerType", 	appetizerPrice: 50, 	imgLocation:""},
	{id:3, appetizerName: "Vegetable Soup", appetizerDesc: "soup",		type: "appetizerType", 	appetizerPrice: 40, 	imgLocation:""},
	{id:4, appetizerName: "Kinpira Gobo", 	appetizerDesc: "sideDish", 	type: "appetizerType",	appetizerPrice: 70, 	imgLocation:""},
	{id:5, appetizerName: "Chicken Katsu", 	appetizerDesc: "mainDish", 	type: "appetizerType",	appetizerPrice: 110, imgLocation:""},
];
/**
* The array that will hold the list of 'desserts' that 
* are statically added in the application.
* The items for the array are mocked.
**/
var dessertArray = [
	{id:0, dessertName: "Beef Teriyaki", 	dessertDesc: "mainDish", type: "dessertType",dessertPrice:100, imgLocation:""},
	{id:1, dessertName: "Onigiri", 	dessertDesc: "sideDish",type: "dessertType", dessertPrice: 80, imgLocation:""},
	{id:2, dessertName: "Miso Soup", dessertDesc: "soup",type: "dessertType", dessertPrice: 50, imgLocation:""},
	{id:3, dessertName: "Vegetable Soup", dessertDesc: "soup",type: "dessertType", dessertPrice: 40, imgLocation:""},
	{id:4, dessertName: "Kinpira Gobo", dessertDesc: "sideDish", type: "dessertType",dessertPrice: 70, imgLocation:""},
	{id:5, dessertName: "Chicken Katsu", dessertDesc: "mainDish", type: "dessertType",dessertPrice: 110, imgLocation:""},
];

/**
* The array that will hold the list of 'menus' that 
* are dynamically added in the application.
**/
var menuItemArray = [];

/**
* The screen that is currently displayed.
**/
var displayedScreen = "";


/*
* This function is called to change the displayed div,
* label and image of the div#content depending on the selected
* 'displayScreen'.
* @param displayScreen - 'id' of the div to be displayed
*/
function changeContent(displayScreen) {
	console.log("restoMain.changeContent" + " start");
	console.log("restoMain.changeContent panel: " + displayScreen)
	if (displayedScreen != displayScreen) {
		var lblHeader = "";
		var iconHeader = "";
		
		document.getElementById("contentHome").style.visibility = "hidden";
		document.getElementById("contentHome").style.display = "none";
		document.getElementById("screenTable").style.visibility = "hidden";
		document.getElementById("screenTable").style.display = "none";
		document.getElementById("screenOrder").style.visibility = "hidden";
		document.getElementById("screenOrder").style.display = "none";
		document.getElementById("screenMenu").style.visibility = "hidden";
		document.getElementById("screenMenu").style.display = "none";
				
		switch (displayScreen) {
			case "screenTable":
				lblHeader = " Table ";
				iconHeader = "..\\assets\\table_icon.png";
				// changing the content of the body, displaying the Table screen
				document.getElementById("screenTable").style.visibility = "visible";
				document.getElementById("screenTable").style.display = "block";
				initAdminTableScreen();
				break;
			case "screenOrder":
				lblHeader = " Order ";
				iconHeader = "..\\assets\\order_icon.png";
				// changing the content of the body, displaying the customize screen
				document.getElementById("screenOrder").style.visibility = "visible";
				document.getElementById("screenOrder").style.display = "block";
				initAdminOrderScreen();
				break;
			case "screenMenu":
				lblHeader = " Menu ";
				iconHeader = "..\\assets\\menu_icon.png";
				// changing the content of the body, displaying the menu screen
				document.getElementById("screenMenu").style.visibility = "visible";
				document.getElementById("screenMenu").style.display = "block";
				initAdminMenuScreen();
				break;
			default:
				break;
		}
		
		displayedScreen = displayScreen;
		// clearing the message spanMessage
		document.getElementById("spanMessage").innerHTML = "";
		
		// Change the label and the image of the header.
		document.getElementById("headerLabel").innerHTML = lblHeader;
		document.getElementById("headerIcon").src = iconHeader;
	}
	console.log("restoMain.changeContent" + " start");
}

/*Loregge Kandt codes*/
function openLogin(){
	window.location.href = 'login_screen.html';
}

function signout(){
	window.location.href = 'customer_screen.html';
}


function check()/*function to check userid & password*/
{
	console.log("restoMain.check" + " start");
	var userid = document.getElementById("username");
	var psword = document.getElementById("password");
	 /*the following code checks whether the entered userid and password are matching*/
	 
	 if( userid.value == "admin" && psword.value == "okradol")
	  {
		window.location.href = 'admin_screen.html';
	  }
	 else
	 {
	   alert("Error Password or Username")/*displays error message*/
	  }
	  console.log("restoMain.check" + " end");
}

function openModal(section, product){
	console.log("restoMain.openModal" + " start");
	document.getElementById("cartSection").style.visibility = "hidden";
	document.getElementById("cartSection").style.display = "none";
	document.getElementById("menuItemSection").style.visibility = "hidden";
	document.getElementById("menuItemSection").style.display = "none";
	
	// Get the modal
	var modal = document.getElementById('myModal');

	// Get the button that opens the modal
	var cartBtn = document.getElementById("cartBtn");
	var imageBtn = document.getElementById("myBtn");
	
	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks on the button, open the modal 
	modal.style.display = "block";
	switch(section){
		case "cart": 
				document.getElementById("cartSection").style.visibility = "visible";
				document.getElementById("cartSection").style.display = "block";
				// GEE: added these codes
				var btnID = document.createElement('button');
				//var btnID = document.getElementsByClassName('modalBtn');
				btnID.setAttribute("class", "modalBtn");
				btnID.innerHTML = "Add To Tray";
				//alert("product" + product.id);
				btnID.id = product.id;
				//alert("button" + btnID.id);
				btnID.onclick = function(){
									addToTray(this);
				}
				var divForm = document.getElementById('menuItemSection');				
					divForm.appendChild(btnID);
				//
				break;
		case "drinkModal": 
				document.getElementById("menuItemSection").style.visibility = "visible";
				document.getElementById("menuItemSection").style.display = "block";
				//this section should change whenever a user selects different kind of drinks
				//this would be the image section --- to be implemented
				//var drinkImage = document.createElement('IMG');
				document.getElementById("prodImage").src = drinkArray[product.id].imgLocation;
				document.getElementById("productName").innerHTML = drinkArray[product.id].drinkName;
				document.getElementById("productDesc").innerHTML = drinkArray[product.id].drinkDesc;
				document.getElementById("productPrice").innerHTML = drinkArray[product.id].drinkPrice;
				var btnID = document.createElement('button');
				//var btnID = document.getElementsByClassName('modalBtn');
				btnID.setAttribute("class", "modalBtn");
				btnID.innerHTML = "Add To Tray";
				//alert("product" + product.id);
				btnID.id = product.id;
				//alert("button" + btnID.id);
				btnID.onclick = function(){
									addToTray(this);
				}
				var divForm = document.getElementById('menuItemSection');				
					divForm.appendChild(btnID);
				break;
		case "dishModal": 
				document.getElementById("menuItemSection").style.visibility = "visible";
				document.getElementById("menuItemSection").style.display = "block";
				//this section should change whenever a user selects different kind of dishes
				//this would be the image section --- to be implemented
				document.getElementById("prodImage").src = dishArray[product.id].imgLocation;
				document.getElementById("productName").innerHTML = dishArray[product.id].dishName;
				document.getElementById("productDesc").innerHTML =	 dishArray[product.id].dishDesc;
				document.getElementById("productPrice").innerHTML = dishArray[product.id].dishPrice;
				var btnID = document.createElement('button');
				//var btnID = document.getElementsByClassName('modalBtn');
				btnID.setAttribute("class", "modalBtn");
				btnID.innerHTML = "Add To Tray";
				//alert("product" + product.id);
				btnID.id = product.id;
				//alert("button" + btnID.id);
				btnID.onclick = function(){
									addToTray(this);
				}
				var divForm = document.getElementById('menuItemSection');				
					divForm.appendChild(btnID);				
				break;
		case "dessertModal": 
				document.getElementById("menuItemSection").style.visibility = "visible";
				document.getElementById("menuItemSection").style.display = "block";
				//this section should change whenever a user selects different kind of dishes
				document.getElementById("prodImage").src = dessertArray[product.id].imgLocation;
				document.getElementById("productName").innerHTML = dessertArray[product.id].dessertName;
				document.getElementById("productDesc").innerHTML =	 dessertArray[product.id].dessertDesc;
				document.getElementById("productPrice").innerHTML = dessertArray[product.id].dessertPrice;
				var btnID = document.createElement('button');
				//var btnID = document.getElementsByClassName('modalBtn');
				btnID.setAttribute("class", "modalBtn");
				btnID.innerHTML = "Add To Tray";
				//alert("product" + product.id);
				btnID.id = product.id;
				//alert("button" + btnID.id);
				btnID.onclick = function(){
									addToTray(this);
				}
				var divForm = document.getElementById('menuItemSection');				
					divForm.appendChild(btnID);				
				break;
		case "appetizerModal": 
				document.getElementById("menuItemSection").style.visibility = "visible";
				document.getElementById("menuItemSection").style.display = "block";
				//this section should change whenever a user selects different kind of dishes
				//this would be the image section --- to be implemented
				document.getElementById("prodImage").src = appetizerArray[product.id].imgLocation;
				document.getElementById("productName").innerHTML = appetizerArray[product.id].appetizerName;
				document.getElementById("productDesc").innerHTML =	 appetizerArray[product.id].appetizerDesc;
				document.getElementById("productPrice").innerHTML = appetizerArray[product.id].appetizerPrice;
				var btnID = document.createElement('button');
				//var btnID = document.getElementsByClassName('modalBtn');
				btnID.setAttribute("class", "modalBtn");
				btnID.innerHTML = "Add To Cart";
				//alert("product" + product.id);
				btnID.id = product.id;
				//alert("button" + btnID.id);
				btnID.onclick = function(){
									addToTray(this);
				}
				var divForm = document.getElementById('menuItemSection');				
					divForm.appendChild(btnID);				
				break;
	}
	
		// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		modal.style.display = "none";
		var divForm = document.getElementById('menuItemSection');
		divForm.removeChild(divForm.lastChild);
		//var element = document.getElementsByClassName("modalBtn");
		//element.parentNode.removeChild(element);
	}
	console.log("ok == " + "ok");
	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
			var divForm = document.getElementById('menuItemSection');
		divForm.removeChild(divForm.lastChild);
		}
	}
		
	console.log("restoMain.openModal" + " end");
	
}

