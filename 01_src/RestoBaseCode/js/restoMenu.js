/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities for the 'Admin Menu' screen.
* @author Team Rocket
* @version 0.01
* Version History
* [07/25/2016] 0.01 â€“ Keith Edmond L. Araneta â€“ initial codes
* [07/27/2016] 0.01 â€“ Teresita Tala P. Rabago â€“ added and edited some codes
* [07/27/2016] 0.01 â€“ Keith Edmond L. Araneta â€“ added and edited some codes
* [07/28/2016] 0.01 â€“ Keith Edmond L. Araneta â€“ fixed bugs in the update menu item function
*											  -	implemented delete menu item function
**/

/**
* This function is used to initialize the elements in the 'Admin Menu' screen.
* Clears the input field, sets the 'Menu' table.
* Displays necessary elements needed in the screen.
* Hides unnecessay elements in the screen.
**/
function initAdminMenuScreen() {
	console.log("restoMenu.initAdminMenuScreen" + " start");
	
	document.getElementById("createMenuBtn").style.visibility = "visible";
	document.getElementById("createMenuBtn").style.display = "inline";
	
	document.getElementById("searchField").style.display = "inline";
	document.getElementById("searchField").value = null;
	document.getElementById("searchField").placeholder = "Menu Item Name / Status";
	document.getElementById("searchBtn").style.display = "inline";
	document.getElementById("createMenuForm").style.display = "none";
	document.getElementById("updateMenuForm").style.display = "none";
	document.getElementById("screenMenuForm").style.display = "block";
	
	// removing the other rows for the menu
	removeAllMenuRow();
	
	// adding rows for the menu
	populateMenuTable(); 
	
	console.log("restoMenu.initAdminMenuScreen" + " end");
} 

/**
* This function is used to add rows to the 'menuTbl' table
* from the menu items in the 'menuItemArray'
**/
function populateMenuTable() {
	console.log("restoMenu.populateMenuTable" + " start");
	
	// getting the table element
	var tblMenu = document.getElementById("menuTbl");
	
	for(var i = 0; i < menuItemArray.length; i++) {
		var tableRow = tblMenu.insertRow(tblMenu.rows.length);
		
		// creating the columns for the row
		var tableCell1 = tableRow.insertCell(0);
		var tableCell2 = tableRow.insertCell(1);
		var tableCell3 = tableRow.insertCell(2);
		var tableCell4 = tableRow.insertCell(3);
		var tableCell5 = tableRow.insertCell(4);
		var tableCell6 = tableRow.insertCell(5);
		
		// adding contents to their corresponding column
		tableCell1.innerHTML = menuItemArray[i].name;
		tableCell2.innerHTML = menuItemArray[i].numOfServings;
		tableCell3.innerHTML = menuItemArray[i].price;
		tableCell4.innerHTML = menuItemArray[i].state;
		tableCell5.innerHTML = '<img src=\"..\\assets\\edit_icon2.png\" alt=\"Edit Icon\" onclick=\"initAdminUpdateMenuScreen(this)\" />';
		tableCell6.innerHTML = '<img src=\"..\\assets\\delete_icon2.png\" alt=\"Delete Icon\" onclick=\"deleteMenu(this)\" />';
	}
	
	console.log("restoMenu.populateMenuTable" + " end");
}

/**
* This function is used to initialize the elements in the 'Create Menu Item' screen.
* Hides unnecessay elements in the screen.
**/
function initAdminCreateMenuScreen() {
	console.log("restoMenu.initAdminCreateMenuScreen" + " start");
	
	document.getElementById("browseImageBtnCreate").value = null;
	document.getElementById("menuNameField").value = null;
	document.getElementById("menuPriceField").value = null;
	document.getElementById("imageBoxCreate").src = "";
	
	document.getElementById("createMenuBtn").style.visibility = "hidden";
	document.getElementById("createMenuBtn").style.display = "none";
	document.getElementById("searchField").style.display = "none";
	document.getElementById("searchBtn").style.display = "none";
	document.getElementById("screenMenuForm").style.display = "none";
	document.getElementById("createMenuForm").style.display = "block"; 
	
	console.log("restoMenu.initAdminCreateMenuScreen" + " end");
}

/**
* This function is used to initialize the elements in the 'Update Menu Item' screen.
* Hides unnecessay elements in the screen.
**/
function initAdminUpdateMenuScreen(clicked) {
	console.log("restoMenu.initAdminUpdateMenuScreen" + " start");
	
	document.getElementById("browseImageBtnUpdate").value = null;
	document.getElementById("menuNameFieldUpdate").value = null;
	document.getElementById("menuPriceFieldUpdate").value = null;
	document.getElementById("menuServingsFieldUpdate").value = null;
	document.getElementById("imageBoxUpdate").src = "";
	
	document.getElementById("createMenuBtn").style.visibility = "hidden";
	document.getElementById("createMenuBtn").style.display = "none";
	document.getElementById("searchField").style.display = "none";
	document.getElementById("searchBtn").style.display = "none";
	document.getElementById("screenMenuForm").style.display = "none";
	document.getElementById("updateMenuForm").style.display = "block";
	
	populateUpdateMenuFields(clicked);
	
	console.log("restoMenu.initAdminUpdateMenuScreen" + " end");
}

/**
* This function is used to display information about the selected menu item to be updated
**/
function populateUpdateMenuFields(clicked) {
	console.log("restoMenu.populateUpdateMenuFields" + " start");
	
	clickedIndex = clicked.parentNode.parentNode.rowIndex - 1;
	var menuItem = menuItemArray[clickedIndex];
	document.getElementById("imageBoxUpdate").src = "..\\assets\\Menu Item Images\\" + menuItem.logo;
	document.getElementById("menuNameFieldUpdate").value = menuItem.name;
	document.getElementById("menuPriceFieldUpdate").value = menuItem.price;
	document.getElementById("menuServingsFieldUpdate").value = menuItem.numOfServings;
	//document.getElementById("browseImageBtnUpdate").value = '"C:\fakepath\filePath\"' + menuItem.logo;
	
	console.log("restoMenu.populateUpdateMenuFields" + " end");
}

/**
* This function is called to validate if the inputted 'name' of the menu item
* is already in the list.
* @return true - name does not exists in the list; false - otherwise
**/
function validateMenuItemNameExists(id) {
	console.log("restoMenu.validateMenuItemNameExists" + "  start");
	
	var valid = true;
	var menuItemNameField = document.getElementById(id).value;
	
	// Note: The 'required' field is automatically validated in the html file.	
	// Traverse through the 'menuItemArray' to check if there exists an object with the same name
	for (var i = 0; i < menuItemArray.length; i++){
		if(id == "menuNameField") {
			if (menuItemNameField.toLowerCase() == menuItemArray[i].name.toLowerCase()) {
				alert("Name already exists.");
				valid = false;
				break;
			}
		}
		else if (id == "menuNameFieldUpdate" && menuItemArray[clickedIndex].name != menuItemNameField && menuItemNameField.toLowerCase() == menuItemArray[i].name.toLowerCase()) {
			alert("Name already exists.");
			valid = false;
			break;
		}
	}
	
	return valid;
	
	console.log("restoMenu.validateMenuItemNameExists" + "  end");
}

/**
* Used to remove all the rows in the 'menuTbl' table,
* except for the header (row 1)
**/
function removeAllMenuRow() {
	console.log("restoMenu.removeAllMenuRow" + "  start");
	var table = document.getElementById("menuTbl");
	var lastRow = table.rows.length;
	
	// deleting everything else except for the table header
	for (var rowIndex = 1; rowIndex < lastRow;) {
		table.deleteRow(rowIndex);
		// updating the lastRow number because there was a deletion
		lastRow = table.rows.length;
	}
	
	console.log("restoMenu.removeAllMenuRow" + "  end");
}

/**
* This function is used to add the 'menuItem' into the 'menuItemArray' and add it to the 'menuTbl' table.
**/
function registerMenu() {
	console.log("restoMenu.registerMenu" + " start");
	
	// displaying a confirmation message to the user and storing its result to a variable
	var confirmation = window.confirm("Are you sure you want to add menu?");
	if (confirmation) {
		
		if (validateMenuItemNameExists("menuNameField")) {
			var nameField = document.getElementById("menuNameField");
			var priceField = document.getElementById("menuPriceField");
			//returning the fake path of the image "C:\fakepath\filePath"
			var fileFakePath = document.getElementById("browseImageBtnCreate").value;
			//splitting the fake path to get the file name of the image 
			var fileStr = fileFakePath.split("\\");
			var fileName = fileStr[2];
			
			// storing the values of the each corresponding field into an object
			var menuItem = { 
				id: menuItemArray.length, 
				name: nameField.value,
				numOfServings: 0,
				price: priceField.value,
				state: "Unavailable",
				logo: fileName
			};
			
			// Inserting the menu item into the 'menuItemArray'
			menuItemArray.push(menuItem);
			alert("Adding item to 'menuItemArray' was successful.");
			
			for (var i = 0; i < menuItemArray.length; i++) {
				console.log(menuItemArray[i]);
			}

			// initializing the screen to its original state
			initAdminMenuScreen();
		}
	}
	return false;
	console.log("restoMenu.registerMenu" + " end");
}

/**
* This function is used to preview the chosen image for the menu item
**/
function previewImage(uploadBtnId,imgBoxId) {
	//returning the fake path of the image "C:\fakepath\filePath"
	var fileFakePath = document.getElementById(uploadBtnId).value;
	//splitting the fake path to get the file name of the image 
	var fileStr = fileFakePath.split("\\");
	var fileName = fileStr[2];
	var logo = document.getElementById(imgBoxId).src = "..\\assets\\Menu Item Images\\" + fileName;
}

/**
* This function is used to update a 'menuItem' in the 'menuItemArray'.
**/
function updateMenu() {
	console.log("restoMenu.updateMenu" + " start");
	
	// displaying a confirmation message to the user and storing its result to a variable
	var confirmation = window.confirm("Are you sure you want to update menu?");
	if (confirmation) {
		
		if (validateMenuItemNameExists("menuNameFieldUpdate")) {
			var nameField = document.getElementById("menuNameFieldUpdate");
			var priceField = document.getElementById("menuPriceFieldUpdate");
			var numOfServingsField = document.getElementById("menuServingsFieldUpdate");
			// returning the fake path of the image "C:\fakepath\filePath"
			var fileFakePath = document.getElementById("browseImageBtnUpdate").value;
			// splitting the fake path to get the file name of the image 
			var fileStr = fileFakePath.split("\\");
			var fileName = fileStr[2];
			
			// setting the value of each attribute
			menuItemArray[clickedIndex].name = nameField.value;
			menuItemArray[clickedIndex].price = priceField.value;
			menuItemArray[clickedIndex].numOfServings = numOfServingsField.value;
			if(menuItemArray[clickedIndex].numOfServings > 0) 
				menuItemArray[clickedIndex].state = "Available";
			else
				menuItemArray[clickedIndex].state = "Unavailable";
			menuItemArray[clickedIndex].logo = fileName;
		
			alert("Updating menu item was successful.");
			
			// initializing the screen to its original state
			initAdminMenuScreen();
		}
	}
	
	return false;
	console.log("restoMenu.updateMenu" + " end");
}

/**
* This function is used to delete the 'menuItem' from the 'menuItemArray' and remove it from the 'menuTbl' table.
**/
function deleteMenu(clicked) {
	console.log("restoMenu.deleteMenu" + " start");
	
	// displaying a confirmation message to the user and storing its result to a variable
	var confirmation = window.confirm("Are you sure you want to delete menu?");
	if (confirmation) {
		var index = clicked.parentNode.parentNode.rowIndex;
		var table = document.getElementById("menuTbl");
		table.deleteRow(index);
		
		// removing menuItem from the 'menuItemArray'
		menuItemArray.splice(index-1, 1);
		
		alert("Deleting menu item was successful.");
		
		// initializing the screen to its original state
		initAdminMenuScreen();
	}
	console.log("restoMenu.deleteMenu" + " end");
}