/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Team Rocket - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities and global variables for the application.
* @author Team Rocket
* @version 0.01
* Version History
* [07/26/2016] 0.01 – Ryan L. Lobitana – initial codes
**/

/**
* This function will set the header of the content-screen div
**/
function btnAppetizer(){
	var header = document.getElementById('content-header');
	header.innerHTML = "Appetizer";
	populateAppetizerImage();
	
}
/**
* Populating the div and its respective images
* Setting of the necessary attributes accompanied in each element
**/
function populateAppetizerImage() {
	console.log("Populating the appetizer images");
	
	//var contentScreen = document.getElementsByTagName('content-screen');
	//create a div with an image inside every loop of the appetizeerArray
	for(var i=0; i<appetizerArray.length; i++){
		var imageLocation = "..\\assets\\";
		//alert("blabla" + i);
		//creating the div element
		var iDiv = document.createElement('div');
		iDiv.className = "dish-content dish-left";
		iDiv.id = 'myBtn';
		iDiv.value = "val1";
		//appending the div to content-screen
		document.getElementsByClassName('content-screen')[0].appendChild(iDiv);
		//document.getElements
		
		//creating the label wla pay ccs
		//var drinkLabel = document.createElement('label');
		//drinkLabel.innerHTML = "" + drinkArray[i].drinkName;
		//iDiv.appendChild(drinkLabel);
		
		//creating the image element
		var appertizerImage = document.createElement('IMG');
		//drinkImage.className = "drinkType";
		appertizerImage.id = i;
		//drinkImage.value = "val" + i;
		//add the event on every image
		appertizerImage.onclick = function() {
								openModal('appetizerModal', this);
							};
		//setting the image path accordingly
		
		switch (i) {
			case 0:
				imageLocation += "drink1.jpg";
				//drinkImage.setAttribute("src","..\\assets\\drink1.jpg");
				break;
			case 1:
				imageLocation += "shake.jpg";
				//drinkImage.setAttribute("src","..\\assets\\shake.jpg");
				break;
			case 2:
				imageLocation += "drink1.jpg";
				//drinkImage.setAttribute("src","..\\assets\\drink1.jpg");
				break;
			case 3:
				imageLocation += "shake.jpg";
				//drinkImage.setAttribute("src","..\\assets\\shake.jpg");
				break;
			case 4:
				imageLocation += "drink1.jpg";
				//drinkImage.setAttribute("src","..\\assets\\drink1.jpg");
				break;
			case 5:
				imageLocation += "drink1.jpg";				
				//drinkImage.setAttribute("src","..\\assets\\shake.jpg");
				break;
		}
		appetizerArray[i].imgLocation = imageLocation;
		appertizerImage.src = imageLocation;
		//appending the drinkImage to the iDiv
		iDiv.appendChild(appertizerImage);
	}
	console.log("End of Drink Array");
}